﻿namespace ExpertSystemShell
{
    using System.Linq;
    using System.Windows.Forms;

    using ExpertSystemShell.Entities;

    public partial class RuleForm : Form
    {
        private readonly KnowledgeBase _knowledgeBase;

        private readonly Rule _rule;

        private bool _isEditName = false;

        private bool _isChanged = true;

        private string _oldRuleName;

        private string _errorText;

        private AddEditVariableForm _variableForm;

        public RuleForm(KnowledgeBase knowledgeBase, int? order)
        {
            _knowledgeBase = knowledgeBase;
            InitializeComponent();
            tbNewRule.Text = $"Rule_{_knowledgeBase.Rules?.Count}";
            tbNewRule.Focus();
            InitializeVariables();
            _rule = new Rule(tbNewRule.Text);
            tbNewRule.Enabled = false;
            if (order.HasValue)
            {
                _rule.Order = order.Value + 1;
            }
            else
            {
                _rule.Order = 0;
            }
        }

        public RuleForm(KnowledgeBase knowledgeBase, Rule rule)
        {
            _knowledgeBase = knowledgeBase;
            _rule = new Rule(rule);
            _rule.Original = rule;
            InitializeComponent();
            InitializeRule();
            InitializeVariables();
            tbNewRule.Enabled = false;
        }

        #region Init

        private void InitializeRule()
        {
            tbNewRule.Text = _rule.Name;
            tbExplaining.Text = _rule.Reason;
            InitializePremises();
            InitializeConclusions();
        }

        private void InitializePremises()
        {
            dgvPremises.Rows.Clear();
            foreach (var premise in _rule.Premises)
            {
                var row = new DataGridViewRow();
                row.CreateCells(dgvPremises, premise.PremiseContent);
                row.Tag = premise;
                dgvPremises.Rows.Add(row);
            }
        }

        private void InitializeConclusions()
        {
            dgvConclusion.Rows.Clear();
            foreach (var conclusion in _rule.Conclusions)
            {
                var row = new DataGridViewRow();
                row.CreateCells(dgvConclusion, conclusion.ConclusionContent);
                row.Tag = conclusion;
                dgvConclusion.Rows.Add(row);
            }
        }

        private void InitializeVariables(string premiseVariableName = null, string conclusionVariableName = null)
        {
            cbPremiseVariable.Items.Clear();
            //cbPremiseVariable.Items.Add(" ");
            cbConclusionVariable.Items.Clear();
            //cbConclusionVariable.Items.Add(" ");
            if (_knowledgeBase.Variables.Any())
            {
                cbPremiseVariable.Items.AddRange(_knowledgeBase.Variables.Select(v => v.Name).ToArray());
                cbConclusionVariable.Items.AddRange(_knowledgeBase.Variables.Where(x => x.Type != tVariableType.Requested).Select(v => v.Name).ToArray());
            }

            if (premiseVariableName != null)
            {
                cbPremiseVariable.SelectedItem = premiseVariableName;
            }
            else
            {
                cbPremiseVariable.SelectedIndex = 0;
            }
            if (conclusionVariableName != null)
            {
                cbConclusionVariable.SelectedItem = conclusionVariableName;
            }
            else
            {
                cbConclusionVariable.SelectedIndex = 0;
            }
        }

        private void InitializePremiseValues(string domainName)
        {
            cbPremiseValue.Items.Clear();
            if (!string.IsNullOrWhiteSpace(domainName))
            {
                var domain = _knowledgeBase.GetDomain(domainName);
                cbPremiseValue.Items.AddRange(domain.Values.Select(v => v.Value).ToArray());
            }
            else
            {
                cbPremiseValue.SelectedIndex = 0;
            }
        }

        private void InitializeConclusionValues(string domainName)
        {
            cbConclusionValue.Items.Clear();
            if (!string.IsNullOrWhiteSpace(domainName))
            {
                var domain = _knowledgeBase.GetDomain(domainName);
                cbConclusionValue.Items.AddRange(domain.Values.Select(v => v.Value).ToArray());
            }
            else
            {
                cbConclusionValue.SelectedIndex = 0;
            }
        }

        private void InitNewPremiseVariable(Variable variable)
        {
            if (variable != null)
            {
                _knowledgeBase.Variables.Add(variable);
                InitializeVariables();
                cbPremiseVariable.SelectedItem = variable.Name;
            }
            else
            {
                InitializeVariables(cbPremiseVariable.SelectedItem?.ToString());
            }
        }

        private void InitNewConclusionVariable(Variable variable)
        {
            if (variable != null)
            {
                _knowledgeBase.Variables.Add(variable);
                InitializeVariables();
                cbConclusionVariable.SelectedItem = variable.Name;
            }
            else
            {
                InitializeVariables(cbPremiseVariable.SelectedItem?.ToString(), cbConclusionVariable.SelectedItem?.ToString());
            }
        }

        private Variable InitNewVariableWindow()
        {
            _variableForm = new AddEditVariableForm(_knowledgeBase, _knowledgeBase.Variables);
            _variableForm.ShowDialog();
            var newVariable = _variableForm.NewVariable;
            _variableForm.Dispose();
            return newVariable;
        }

        #endregion

        #region Click events

        private void btnAddPremise_Click(object sender, System.EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(cbPremiseVariable.SelectedItem?.ToString())
                && !string.IsNullOrWhiteSpace(cbPremiseValue.SelectedItem?.ToString()))
            {
                AddPremise(
                    cbPremiseVariable.SelectedItem.ToString(), 
                    cbPremiseValue.SelectedItem.ToString());
                _isChanged = true;
                cbPremiseVariable.SelectedIndex = 0;
            }
            else
            {
                MessageBox.Show("Не заполнены все необходимые поля у новой посылки!");
            }
        }

        private void btnAddConclusion_Click(object sender, System.EventArgs e)
        {

            if (!string.IsNullOrWhiteSpace(cbConclusionVariable.SelectedItem?.ToString())
                && !string.IsNullOrWhiteSpace(cbConclusionValue.SelectedItem?.ToString()))
            {
                AddConclusion(
                    cbConclusionVariable.SelectedItem.ToString(),
                    cbConclusionValue.SelectedItem.ToString());
                _isChanged = true;
                cbConclusionVariable.SelectedIndex = 0;
            }
            else
            {
                MessageBox.Show("Не заполнены все необходимые поля у нового заключения!");
            }
        }

        private void btnDeletePremise_Click(object sender, System.EventArgs e)
        {
            DeletePremise((Fact)dgvPremises.CurrentRow?.Tag);
            _isChanged = true;
        }

        private void btnDeleteConclusion_Click(object sender, System.EventArgs e)
        {
            DeleteConclusion((Fact)dgvConclusion.CurrentRow?.Tag);
            _isChanged = true;
        }

        private void btnEdit_Click(object sender, System.EventArgs e)
        {
            if (!_isEditName)
            {
                tbNewRule.Enabled = true;
                btnEdit.Text = "OK";
                _isEditName = true;
                _oldRuleName = _rule.Name;
            }
            else
            {
                EditRuleName(tbNewRule.Text);
                if (_isEditName)
                {
                    return;
                }

                tbNewRule.Enabled = false;
                btnEdit.Text = "Изменить";
            }

            _isChanged = true;
        }

        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnSave_Click(object sender, System.EventArgs e)
        {
            if (CheckRule())
            {
                _knowledgeBase.AddRule(_rule);
                _isChanged = false;
                MessageBox.Show("Сохранено!");
            }
            else
            {
                MessageBox.Show(_errorText);
            }
        }

        private void btnAddPremiseVariable_Click(object sender, System.EventArgs e)
        {
            InitNewPremiseVariable(InitNewVariableWindow());
            InitializePremises();
            InitializeConclusions();
            if (!_knowledgeBase.Rules.Contains(_rule.Original))
            {
                _isChanged = false;
                Close();
            }
        }

        private void btnAddConclusionVariable_Click(object sender, System.EventArgs e)
        {
            InitNewConclusionVariable(InitNewVariableWindow());
        }

        #endregion

        #region ADD

        private void AddPremise(string variableName, string valueName)
        {
            var variable = _knowledgeBase.GetVariable(variableName);
            var value = variable.GetDomainValue(valueName);
            var fact = new Fact(variable, value);
            fact.Order = _rule.Premises.Count;
            _rule.AddPremise(fact);
            var row = new DataGridViewRow();
            row.Tag = fact;
            row.CreateCells(dgvPremises, fact.PremiseContent);
            dgvPremises.Rows.Add(row);
        }

        private void AddConclusion(string variableName, string valueName)
        {
            var variable = _knowledgeBase.GetVariable(variableName);
            var value = variable.GetDomainValue(valueName);
            var fact = new Fact(variable, value);
            _rule.AddConclusion(fact);
            var row = new DataGridViewRow();
            row.Tag = fact;
            row.CreateCells(dgvConclusion, fact.ConclusionContent);
            dgvConclusion.Rows.Add(row);
        }

        #endregion

        #region DELETE

        private void DeletePremise(Fact fact)
        {
            if (fact != null)
            {
                dgvPremises.Rows.Remove(dgvPremises.CurrentRow);
                _rule.DeletePremise(fact.Variable.Name, fact.Value.Value);
                for (int i = 0; i < dgvPremises.RowCount; i++)
                {
                    var premise = (Fact)dgvPremises.Rows[i].Tag;
                    premise.Order = i;
                }
            }
        }

        private void DeleteConclusion(Fact fact)
        {
            if (fact != null)
            {
                dgvConclusion.Rows.Remove(dgvConclusion.CurrentRow);
                _rule.DeleteConclusion(fact.Variable.Name, fact.Value.Value);
            }
        }

        #endregion

        #region Changed events

        private void cbPremiseVariable_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            var variable = _knowledgeBase.GetVariable(cbPremiseVariable.SelectedItem.ToString());
            if (variable != null)
            {
                InitializePremiseValues(variable.Domain.Name);
                cbPremiseValue.SelectedIndex = 0;
            }
            else
            {
                cbPremiseValue.Items.Clear();
            }
        }

        private void cbConclusionVariable_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            var variable = _knowledgeBase.GetVariable(cbConclusionVariable.SelectedItem.ToString());
            if (variable != null)
            {
                InitializeConclusionValues(variable.Domain.Name);
                cbConclusionValue.SelectedIndex = 0;
            }
            else
            {
                cbConclusionValue.Items.Clear();
            }
        }

        private void tbExplaining_TextChanged(object sender, System.EventArgs e)
        {
            _rule.Reason = tbExplaining.Text;
        }

        #endregion

        #region Closing events

        private void RuleForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_isChanged)
            {
                var result = MessageBox.Show(
                    "Сохранить изменения?",
                    "Внимание",
                    MessageBoxButtons.YesNoCancel);

                if (result == DialogResult.Yes)
                {
                    if (CheckRule())
                    {
                        _knowledgeBase.AddRule(_rule);
                    }
                    else
                    {
                        MessageBox.Show(_errorText);
                        e.Cancel = true;
                    }
                }

                e.Cancel = result == DialogResult.Cancel;
            }
        }

        #endregion



        #region EDIT

        private void EditRuleName(string newRuleName)
        {
            if (newRuleName != _oldRuleName)
            {
                var rule = _knowledgeBase.GetRule(newRuleName);
                if (rule != null && rule != _rule.Original)
                {
                    MessageBox.Show("Правило с таким именем уже существует!");
                }
                else
                {
                    _rule.Name = newRuleName;
                    _isEditName = false;
                }
            }
            else
            {
                _isEditName = false;
            }
        }

        #endregion

        #region Check

        private bool CheckRule()
        {
            _errorText = "При сохранении правила были выявлены следующие ошибки:\r\n";
            bool isSuccess = true;
            int i = 1;
            if (string.IsNullOrWhiteSpace(tbNewRule.Text))
            {
                _errorText += $"{i++}. Правило не может состоять из пробелов и должно быть длиной хотя бы 1 символ.\r\n";
                isSuccess = false;
            }

            var ruleFromBase = _knowledgeBase.GetRule(tbNewRule.Text);
            if (ruleFromBase != null
                && ruleFromBase != _rule.Original)
            {
                _errorText += $"{i++}. Правило с таким именем уже существует.\r\n";
                isSuccess = false;
            }

            if (!_rule.Premises.Any())
            {
                _errorText += $"{i++}. Правило не содержит ни одной посылки.\r\n";
                isSuccess = false;
            }

            if (!_rule.Conclusions.Any())
            {
                _errorText += $"{i++}. Правило не содержит ни одного заключения.\r\n";
                isSuccess = false;
            }

            if (string.IsNullOrWhiteSpace(tbExplaining.Text))
            {
                _errorText += $"{i++}. Правило не содержит объяснения.";
                isSuccess = false;
            }

            return isSuccess;
        }

        #endregion
    }
}
