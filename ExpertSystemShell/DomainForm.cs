﻿namespace ExpertSystemShell
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Windows.Forms;

    using ExpertSystemShell.Entities;

    public partial class DomainForm : Form
    {
        private KnowledgeBase _knowledgeBase;

        private readonly bool _fromVariableForm;

        private DataGridViewCell _currentCell;

        private string _editingDomainName;

        private string _editingValue;

        private List<Domain> _domains = new List<Domain>();

        private bool _isChanged = false;

        public Domain NewDomain { get; set; }

        public DomainForm(KnowledgeBase knowledgeBase, bool fromVariableForm = false)
        {
            _knowledgeBase = knowledgeBase;
            InitDomainList();
            _fromVariableForm = fromVariableForm;
            InitializeComponent();
            btnAddDomain.Enabled = false;
            btnAddValue.Enabled = false;
            InitializeDomains();
            InitializeValues(_knowledgeBase.Domains.FirstOrDefault()?.Name);
            InitNewDomainName();
            if (fromVariableForm)
            {
                ActiveControl = tbNewDomain;
                tbNewDomain.Focus();
            }
        }

        #region ADD

        private void AddDomain(string domainName)
        {
            if (_domains.FirstOrDefault(d => d.Name == domainName) != null)
            {
                MessageBox.Show("Домен с таким именем уже существует!");
                return;
            }

            var row = new DataGridViewRow();
            domainName = domainName.Trim();
            if (string.IsNullOrEmpty(domainName))
            {
                MessageBox.Show("Домен не должен состоять только из пробелов!");
                return;
            }
            row.CreateCells(dgvDomain, domainName);
            var newDomain = new Domain(domainName);
            row.Tag = newDomain;
            dgvDomain.Rows.Add(row);
            _domains.Add(newDomain);
            tbNewDomain.Clear();
            dgvDomain.CurrentCell = row.Cells[0];
            _isChanged = true;

            if (_fromVariableForm)
            {
                NewDomain = newDomain;
            }

            InitNewDomainName();
            InitializeValues(((Domain)dgvDomain.Rows[dgvDomain.RowCount - 1].Tag)?.Name);
            ActiveControl = tbNewValue;
        }

        private void AddDomainValue(string domainName, string value)
        {
            var domain = _domains.FirstOrDefault(d => d.Name == domainName);
            if (domain?.GetValue(value) != null)
            {
                MessageBox.Show("Такое значение уже существует в данном домене!");
                return;
            }

            value = value.Trim();
            if (string.IsNullOrEmpty(value))
            {
                MessageBox.Show("Значение домена не может состоять из одних пробелов!");
                return;
            }

            var row = new DataGridViewRow();
            row.CreateCells(dgvValues, value);
            var newValue = new DomainValue(value);
            row.Tag = newValue;
            dgvValues.Rows.Add(row);
            domain.AddValue(value);
            tbNewValue.Clear();
            dgvValues.CurrentCell = row.Cells[0];
            _isChanged = true;
        }

        #endregion

        #region Changed events

        private void tbNewValue_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tbNewValue.Text))
            {
                btnAddValue.Enabled = false;
            }
            else
            {
                btnAddValue.Enabled = true;
            }
        }

        private void dgvDomain_SelectionChanged(object sender, EventArgs e)
        {
            InitializeValues(dgvDomain.CurrentCell?.Value?.ToString());
            //
        }

        private void tbNewDomain_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tbNewDomain.Text))
            {
                btnAddDomain.Enabled = false;
            }
            else
            {
                btnAddDomain.Enabled = true;
            }
        }
        #endregion

        #region DELETE

        private void DeleteDomain(string domainName)
        {
            DialogResult result = DialogResult.Cancel;
            if (_knowledgeBase.GetRuleByDomain(domainName) != null)
            {
                if (_knowledgeBase.GetRuleByDomain(domainName) != null)
                {
                    result = MessageBox.Show("Этот домен используется в правилах! При изменении будут удалены все правила, где он используется. Продолжить?"
                        , "Внимание", MessageBoxButtons.OKCancel);
                }
            }

            if (dgvDomain.CurrentRow != null
                && result == DialogResult.OK)
            {
                dgvDomain.Rows.Remove(dgvDomain.CurrentRow);
                var domain = _domains.FirstOrDefault(d => d.Name == domainName);
                domain.IsRemoved = true;
                _isChanged = true;
            }
        }

        private void DeleteDomainValue(string domainName, string value)
        {
            if (_knowledgeBase.GetRuleByDomain(domainName) != null)
            {
                MessageBox.Show("Нельзя изменять домен, который используется!");
                return;
            }

            if (dgvValues.CurrentRow != null)
            {
                dgvValues.Rows.Remove(dgvValues.CurrentRow);
                _domains.FirstOrDefault(d => d.Name == domainName)?.DeleteValue(value);
                _isChanged = true;
            }
        }

        #endregion
        
        #region Init

        private void InitializeDomains()
        {
            dgvDomain.Rows.Clear();
            foreach (var domain in _domains)
            {
                if (!domain.IsRemoved)
                {
                    var row = new DataGridViewRow();
                    row.CreateCells(dgvDomain, domain.Name);
                    row.Tag = domain;
                    dgvDomain.Rows.Add(row);
                }
            }

            if (_domains.Count == 0)
            {
                ActiveControl = tbNewDomain;
            }
        }

        private void InitializeValues(string domainName)
        {
            dgvValues.Rows.Clear();
            var domain = _domains.FirstOrDefault(d => d.Name == domainName);
            if (domain != null)
            {
                foreach (var value in domain.Values)
                {
                    var row = new DataGridViewRow();
                    row.CreateCells(dgvValues, value.Value);
                    row.Tag = value;
                    dgvValues.Rows.Add(row);
                }
            }
        }

        private void InitDomainList()
        {
            foreach (var domain in _knowledgeBase.Domains)
            {
                _domains.Add(new Domain(domain.Name, domain.Values, domain));
            }
        }

        private void InitNewDomainName()
        {
            tbNewDomain.Text = $"newDomain{_domains.Count}";
        }

        #endregion

        #region Click events

        private void btnEditValue_Click(object sender, EventArgs e)
        {
            if (!dgvValues.ReadOnly)
            {
                EnableEditingDomain();
                return;
            }

            _currentCell = dgvValues.CurrentCell;
            if (_currentCell != null)
            {
                DisableEditingDomain();
                dgvValues.BeginEdit(true);
                _editingValue = _currentCell.Value.ToString();
            }
        }

        private void btnEditDomain_Click(object sender, EventArgs e)
        {
            if (!dgvDomain.ReadOnly)
            {
                EnableEditingDomainValues();
                return;
            }

            _currentCell = dgvDomain.CurrentCell;
            if (_currentCell != null)
            {
                DisableEditingDomainValues();
                dgvDomain.BeginEdit(true);
                _editingDomainName = _currentCell.Value.ToString();
            }
        }

        private void btnDeleteValue_Click(object sender, EventArgs e)
        {
            DeleteDomainValue(
                ((Domain)dgvDomain.CurrentRow?.Tag)?.Name,
                ((DomainValue)dgvValues.CurrentRow?.Tag)?.Value);
        }

        private void btnAddValue_Click(object sender, EventArgs e)
        {
            AddDomainValue(((Domain)dgvDomain.CurrentRow?.Tag)?.Name, tbNewValue.Text);
        }

        private void btnAddDomain_Click(object sender, EventArgs e)
        {
            AddDomain(tbNewDomain.Text);
            
        }

        private void btnDeleteDomain_Click(object sender, EventArgs e)
        {
            DeleteDomain(((Domain)dgvDomain.CurrentRow?.Tag)?.Name);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        #endregion

        #region Disabling/enabling controls

        private void EnableEditingDomain()
        {
            btnAddValue.Enabled = !string.IsNullOrWhiteSpace(btnAddValue.Text);
            btnAddDomain.Enabled = !string.IsNullOrWhiteSpace(btnAddDomain.Text);
            btnDeleteValue.Enabled = true;
            btnDeleteDomain.Enabled = true;
            btnEditDomain.Enabled = true;
            dgvValues.ReadOnly = true;
            btnEditValue.Image = Image.FromFile(@"C:\Users\User\Documents\Visual Studio 2017\Projects\ExpertSystemShell\ExpertSystemShell\Resources\edit.png");
            toolTip1.SetToolTip(btnEditValue, "Редактировать значение");

        }

        private void DisableEditingDomain()
        {
            btnAddValue.Enabled = !string.IsNullOrWhiteSpace(btnAddValue.Text);
            btnAddDomain.Enabled = !string.IsNullOrWhiteSpace(btnAddDomain.Text);
            btnDeleteValue.Enabled = false;
            btnDeleteDomain.Enabled = false;
            btnEditDomain.Enabled = false;
            dgvValues.ReadOnly = false;
            btnEditValue.Image = Image.FromFile(@"C:\Users\User\Documents\Visual Studio 2017\Projects\ExpertSystemShell\ExpertSystemShell\Resources\okay.png");
            toolTip1.SetToolTip(btnEditValue, "Применить");
        }

        private void DisableEditingDomainValues()
        {
            btnAddValue.Enabled = false;
            btnDeleteValue.Enabled = false;
            btnEditValue.Enabled = false;
            btnAddDomain.Enabled = false;
            btnDeleteDomain.Enabled = false;
            dgvDomain.ReadOnly = false;
            btnEditDomain.Image = Image.FromFile(@"C:\Users\User\Documents\Visual Studio 2017\Projects\ExpertSystemShell\ExpertSystemShell\Resources\okay.png");
            toolTip1.SetToolTip(btnEditDomain, "Применить");
            tbNewDomain.Enabled = false;
            tbNewValue.Enabled = false;
        }

        private void EnableEditingDomainValues()
        {
            btnAddValue.Enabled = true;
            btnDeleteValue.Enabled = true;
            btnEditValue.Enabled = true;
            btnAddDomain.Enabled = true;
            btnDeleteDomain.Enabled = true;
            dgvDomain.ReadOnly = true;
            btnEditDomain.Image = Image.FromFile(@"C:\Users\User\Documents\Visual Studio 2017\Projects\ExpertSystemShell\ExpertSystemShell\Resources\edit.png");
            toolTip1.SetToolTip(btnEditDomain, "Редактировать домен");
            tbNewDomain.Enabled = true;
            tbNewValue.Enabled = true;
        }

        #endregion
        
        #region CellEndEdit

        private void dgvValues_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvValues.CurrentCell.Value != null)
            {
                string str = dgvValues.CurrentCell.Value.ToString();
                if (!string.IsNullOrWhiteSpace(str))
                {
                    string value = str.Trim();
                    _currentCell.Value = value;
                    var domain = (Domain)dgvDomain.Rows[dgvDomain.CurrentRow.Index].Tag;
                    var domainValue = (DomainValue)dgvValues.Rows[_currentCell.RowIndex].Tag;

                    if (value != _editingValue
                        && domain?.GetValue(value) != null)
                    {
                        MessageBox.Show("Такое значение домена уже существует!");
                        dgvValues.BeginEdit(true);
                    }

                    DialogResult result = DialogResult.Abort;

                    if (_knowledgeBase.GetRuleByDomain(domain?.Name) != null)
                    {
                        result = MessageBox.Show("Этот домен используется в правилах! При изменении будут изменены все правила, где он используется. Продолжить?"
                            , "Внимание", MessageBoxButtons.OKCancel);
                    }

                    if ((result == DialogResult.OK
                        || _knowledgeBase.GetRuleByDomain(domain?.Name) == null)
                        && value != _editingValue)
                    {
                        domainValue.Value = value;
                        if (!_domains.Contains(domain))
                        {
                            _domains.Add(domain);
                        }
                        
                        _isChanged = true;

                        //if (result == DialogResult.OK)
                        //{
                        //    EnableEditingDomain();
                        //}
                    }

                    if (result == DialogResult.Cancel)
                    {
                        dgvValues.BeginEdit(true);
                    }
                    else
                    {
                        btnEditValue_Click(null, null);
                    }
                    
                }
                else
                {
                    MessageBox.Show("Значение домена не может состоять только из пробелов!");
                    dgvValues.BeginEdit(true);
                }
            }
            else
            {
                MessageBox.Show("Значение домена не может быть пустым!");
                dgvValues.BeginEdit(true);
            }
        }

        private void dgvDomain_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvDomain.CurrentCell.Value != null)
            {
                string str = dgvDomain.CurrentCell.Value.ToString();
                if (!string.IsNullOrWhiteSpace(str))
                {
                    string name = str.Trim();
                    _currentCell.Value = name;
                    var domain = (Domain)dgvDomain.Rows[_currentCell.RowIndex].Tag;

                    if (_domains.FirstOrDefault(d => d.Name == name) != null
                        && name != _editingDomainName)
                    {
                        MessageBox.Show("Домен с таким именем уже существует!");
                        dgvDomain.BeginEdit(true);
                    }

                    if (name != _editingDomainName)
                    {
                        domain.Name = name;
                        _isChanged = true;
                    }
                }
                else
                {
                    MessageBox.Show("Название домена не может состоять только из пробелов!");
                    dgvDomain.BeginEdit(true);
                }
            }
            else
            {
                MessageBox.Show("Название домена не может быть пустым!");
                dgvDomain.BeginEdit(true);
            }
        }

        #endregion

        #region Key events
        private void tbNewDomain_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AddDomain(tbNewDomain.Text);
            }
        }

        private void tbNewValue_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AddDomainValue(((Domain)dgvDomain.CurrentRow?.Tag)?.Name, tbNewValue.Text);
            }
        }


        #endregion

        #region Closing Events

        private void DomainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_isChanged)
            {
                var result = MessageBox.Show(
                    "Сохранить изменения?",
                    "Внимание",
                    MessageBoxButtons.YesNoCancel);

                if (result == DialogResult.Yes)
                {
                    _knowledgeBase.ReplaceDomains(_domains);
                }

                e.Cancel = result == DialogResult.Cancel;
            }
        }

        #endregion

    }
}
