﻿namespace ExpertSystemShell
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Forms;

    using ExpertSystemShell.Entities;

    public partial class ExplainingForm : Form
    {
        private readonly KnowledgeBase _knowledgeBase;

        private Dictionary<Rule, int> _usedRules;

        private Dictionary<string, DomainValue> _workMemory;

        public ExplainingForm(KnowledgeBase knowledgeBase)
        {
            _knowledgeBase = knowledgeBase;
            InitializeComponent();
            _usedRules = _knowledgeBase.Chain;
            _workMemory = _knowledgeBase.WorkMemory;
            InitView();
            tbBaseName.Text = _knowledgeBase.Name;
            if (_knowledgeBase.Goal == null)
            {
                MessageBox.Show("Консультация не пройдена!");
                Close();
            }
            tbConsultationGoal.Text = _knowledgeBase.Goal.Name;
        }

        #region INIT

        private void InitView()
        {
            lVariables.Items.Clear();
            foreach (var kvp in _workMemory)
            {
                var item = new ListViewItem(new[] { kvp.Key, kvp.Value.Value });
                lVariables.Items.Add(item);
            }
            lVariables.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            Fill(0, null, null);
        }

        private void Fill(int currentLevel, TreeNode parent, Rule parentRule)
        {
            if (currentLevel == 0)
            {
                foreach (var kvp in _usedRules.Where(x => x.Value == 0))
                {
                    var goalNode = new TreeNode($"Цель = {kvp.Key.Conclusions[0].Variable.Name}");
                    var node = new TreeNode($"{kvp.Key.Name}: {kvp.Key.Content}");
                    var reasonNode = new TreeNode($"Причина = {kvp.Key.Reason}");
                    var resultNode = new TreeNode($"{kvp.Key.Conclusions[0].Variable.Name} = {kvp.Key.Conclusions[0].Value.Value}");
                    tvRules.Nodes.Add(goalNode);
                    tvRules.Nodes.Add(node);
                    tvRules.Nodes.Add(reasonNode);
                    tvRules.Nodes.Add(resultNode);
                    Fill(currentLevel + 1, node, kvp.Key);
                }
            }
            else
            {
                foreach (var kvp in _usedRules.Where(x => x.Value == currentLevel))
                {
                    bool isGood = false;
                    foreach (var rule in parentRule.Premises)
                    {
                        foreach (var ruleCons in kvp.Key.Conclusions)
                        {
                            if (rule.ConclusionContent == ruleCons.ConclusionContent)
                            {
                                isGood = true;
                            }
                        }
                    }
                    if (isGood)
                    {
                        var goalNode = new TreeNode($"Цель = {kvp.Key.Conclusions[0].Variable.Name}");
                        var node = new TreeNode($"{kvp.Key.Name}: {kvp.Key.Content}");
                        var reasonNode = new TreeNode($"Причина = {kvp.Key.Reason}");
                        var resultNode = new TreeNode($"{kvp.Key.Conclusions[0].Variable.Name} = {kvp.Key.Conclusions[0].Value.Value}");
                        parent.Nodes.Add(goalNode);
                        parent.Nodes.Add(node);
                        parent.Nodes.Add(reasonNode);
                        parent.Nodes.Add(resultNode);
                        Fill(currentLevel + 1, node, kvp.Key);
                    }
                }
            }
        }

        #endregion

        #region Click events

        private void btnExpand_Click(object sender, EventArgs e)
        {
            tvRules.ExpandAll();
        }

        private void btnCollapse_Click(object sender, EventArgs e)
        {
            tvRules.CollapseAll();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        #endregion

    }
}
