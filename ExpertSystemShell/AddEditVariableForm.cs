﻿namespace ExpertSystemShell
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Forms;

    using ExpertSystemShell.Entities;

    public partial class AddEditVariableForm : Form
    {
        private readonly KnowledgeBase _knowledgeBase;

        private readonly List<Variable> _variables;

        private Variable _newVariable;

        private DomainForm _domainForm;

        private string _errorText;

        public Variable NewVariable { get; set; }

        public AddEditVariableForm(KnowledgeBase knowledgeBase, List<Variable> variables, Variable variable = null)
        {
            _knowledgeBase = knowledgeBase;
            _variables = variables;
            InitializeComponent();
            InitializeDomains();
            if (variable != null)
            {
                _newVariable = variable;
                InitializeVariableProperties(variable);
            }
            else
            {
                _newVariable = new Variable();
                InitNewVariableName();
                rbDerivable.Checked = true;
            }

            ActiveControl = tbVariableName;
        }

        #region INIT

        private void InitNewVariableName()
        {
            tbVariableName.Text = $"newVariable{_variables.Count + 1}";
            _newVariable.Name = tbVariableName.Text;
        }

        private void InitializeVariableProperties(Variable variable)
        {
            if (variable == null)
            {
                return;
            }
            tbVariableName.Text = variable.Name;
            cbDomain.SelectedItem = variable.Domain.Name ?? " ";
            tbQuestion.Text = variable.Question;
            switch (variable.Type)
            {
                case tVariableType.Requested:
                    rbRequested.Checked = true;
                    break;
                case tVariableType.Derivable:
                    rbDerivable.Checked = true;
                    break;
                case tVariableType.DerivableRequested:
                    rbDeriveRequest.Checked = true;
                    break;
            }
        }

        private void InitializeDomains(string domainName = null)
        {
            cbDomain.Items.Clear();
            //cbDomain.Items.Add(" ");
            if (_knowledgeBase.Domains.Any())
            {
                cbDomain.Items.AddRange(_knowledgeBase.Domains.Select(d => d.Name).ToArray());
            }

            if (domainName != null)
            {
                cbDomain.SelectedItem = domainName;
            }
            else
            {
                cbDomain.SelectedIndex = 0;
            }
        }

        private void InitNewDomain(Domain domain)
        {
            if (domain != null)
            {
                cbDomain.SelectedItem = domain.Name;
            }
        }

        private void InitVariable(Variable variable)
        {
            _newVariable = new Variable(variable.Name, variable.Domain, variable.Type, variable.Question, variable);
        }

        private void InitDefaultQuestion(string variableName)
        {
            if (string.IsNullOrWhiteSpace(tbQuestion.Text))
            {
                tbQuestion.Text = $"{variableName}?";
            }
        }

        #endregion

        #region Checking

        private bool CheckVariable()
        {
            string variableName = tbVariableName.Text;
            _errorText = "При сохранении переменной возникли следующие ошибки:\r\n";
            bool isError = false;
            int i = 1;

            if (_variables.FirstOrDefault(v => v.Name == variableName) != null
                && _newVariable.Name != _newVariable.Original?.Name)
            {
                isError = true;
                _errorText += $"{i++}. Переменная с таким именем уже существует!\r\n";
            }

            if (string.IsNullOrWhiteSpace(variableName))
            {
                isError = true;
                _errorText += $"{i++}. Переменная не должна состоять из 0 символов или из одних пробелов!\r\n";
            }

            variableName = variableName.Trim();

            if (string.IsNullOrWhiteSpace(cbDomain.SelectedItem?.ToString()))
            {
                isError = true;
                _errorText += $"{i++}. Не указан домен переменной. \r\n";
                if (!string.IsNullOrWhiteSpace(_newVariable.Domain.Name))
                {
                    cbDomain.SelectedItem = _newVariable.Domain.Name;
                }
            }

            if (!rbDerivable.Checked
                && string.IsNullOrWhiteSpace(tbQuestion.Text))
            {
                isError = true;
                _errorText += $"{i++}. Не указан вопрос для запрашиваемой/выводимо-запрашиваемой переменной.\r\n";
            }

            if (_knowledgeBase.Rules.FirstOrDefault(r => r.Conclusions.Any(c => c.Variable.Name == _newVariable.Original?.Name)) != null
                && rbRequested.Checked)
            {
                isError = true;
                _errorText += $"{i++}. Данная переменная используется в заключениях, она не должна быть запрашиваемой.";
            }

            var rule = _knowledgeBase.GetRuleByVariable(_newVariable.Original?.Name);
            if (rule != null
                && _newVariable.Domain.Name != cbDomain.SelectedItem.ToString())
            {
                isError = true;
                _errorText += $"{i++}. Данная переменная используется в правилах, у нее нельзя поменять домен!\r\n";
                if (!string.IsNullOrWhiteSpace(_newVariable.Domain.Name))
                {
                    cbDomain.SelectedItem = _newVariable.Domain.Name;
                }
            }

            if (isError)
            {
                MessageBox.Show(_errorText);
            }
            else
            {
                _newVariable.Name = variableName;

                if (rbRequested.Checked)
                {
                    _newVariable.Type = tVariableType.Requested;
                }

                if (rbDeriveRequest.Checked)
                {
                    _newVariable.Type = tVariableType.DerivableRequested;
                }

                if (rbDerivable.Checked)
                {
                    _newVariable.Type = tVariableType.Derivable;
                }

                if (_newVariable.Type != tVariableType.Derivable
                    && !string.IsNullOrWhiteSpace(tbQuestion.Text))
                {
                    _newVariable.Question = tbQuestion.Text;
                }

                var domain = _knowledgeBase.GetDomain(cbDomain.SelectedItem.ToString());
                _newVariable.Domain = domain;
            }

            return !isError;
        }

        #endregion

        #region Click events

        private void btnAddDomain_Click(object sender, System.EventArgs e)
        {
            _domainForm = new DomainForm(_knowledgeBase, true);
            _domainForm.ShowDialog();
            var newDomain = _domainForm.NewDomain;
            _domainForm.Dispose();
            InitializeDomains(cbDomain.SelectedItem?.ToString());
            InitNewDomain(newDomain);
        }

        private void btnSaveAndQuit_Click(object sender, System.EventArgs e)
        {
            if (CheckVariable())
            {
                NewVariable = _newVariable;
                DialogResult = DialogResult.OK;
                Close();
            }
        }


        #endregion

        #region Changed events

        private void rbRequested_CheckedChanged(object sender, System.EventArgs e)
        {
            if (rbRequested.Checked)
            {
                InitDefaultQuestion(tbVariableName.Text);
            }
        }

        private void rbDeriveRequest_CheckedChanged(object sender, System.EventArgs e)
        {
            if (rbDeriveRequest.Checked)
            {
                InitDefaultQuestion(tbVariableName.Text);
            }
        }

        #endregion

        private void AddEditVariableForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult != DialogResult.OK)
            {
                DialogResult = DialogResult.Abort;
            }
        }
    }
}
