﻿namespace ExpertSystemShell
{
    using System;
    using System.Windows.Forms;

    using ExpertSystemShell.Entities;

    public partial class ConsultationForm : Form
    {
        /// <summary>
        /// Ответ на вопрос
        /// </summary>
        public DomainValue Answer { get; set; }

        /// <param name="consultGoal">Цель консультации, для отображения</param>
        /// <param name="baseName">Имя БЗ.</param>
        public ConsultationForm(string consultGoal, string baseName)
        {
            InitializeComponent();
            tbGoal.Text = consultGoal;
            tbBaseName.Text = baseName;
        }

        public void AskQuestion(Variable variable)
        {
            cbAnswer.DataSource = null;
            cbAnswer.DataSource = variable.Domain.Values;
            cbAnswer.DisplayMember = "Value";

            if (variable.Type == tVariableType.Requested)
            {
                lBoxConsultation.Items.Add($"Вопрос - {variable.Question}");
            }
            if (variable.Type == tVariableType.DerivableRequested)
            {
                lBoxConsultation.Items.Add($"Вопрос - {variable.Name}?");
            }
        }

        private void btnGoToPrev_Click_1(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btnChoose_Click(object sender, EventArgs e)
        {
            lBoxConsultation.Items.Add($"Ваш ответ - { cbAnswer.Text}");
            Answer = cbAnswer.SelectedItem as DomainValue;
            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Abort;
        }
    }
}
