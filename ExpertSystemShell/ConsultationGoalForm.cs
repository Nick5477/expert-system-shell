﻿namespace ExpertSystemShell
{
    using System;
    using System.Linq;
    using System.Windows.Forms;

    using ExpertSystemShell.Entities;

    public partial class ConsultationGoalForm : Form
    {
        private readonly KnowledgeBase _knowledgeBase;

        public Variable Goal { get; set; }

        public ConsultationGoalForm(KnowledgeBase knowledgeBase)
        {
            _knowledgeBase = knowledgeBase;
            InitializeComponent();
            Goal = new Variable();

            cbGoal.DataSource = _knowledgeBase.Variables.Where(v => v.Type != tVariableType.Requested).ToList();
            cbGoal.DisplayMember = "Name";
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Hide();
            //если задана цель, то можно запускать консультацию
            if (!string.IsNullOrWhiteSpace(cbGoal.SelectedItem?.ToString()))
            {
                Goal = (Variable)cbGoal.SelectedItem;
                _knowledgeBase.Goal = Goal;
                _knowledgeBase.Consult(Goal);
            }
            else
            {
                MessageBox.Show("Не указана цель консультации!", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            Close();
        }

        private void cbGoal_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            Goal = (Variable)cbGoal.SelectedItem;
            _knowledgeBase.Goal = Goal;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
