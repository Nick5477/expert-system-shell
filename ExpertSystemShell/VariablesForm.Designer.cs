﻿namespace ExpertSystemShell
{
    partial class VariablesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VariablesForm));
            this.dgvVariable = new System.Windows.Forms.DataGridView();
            this.VariableName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnAddVariable = new System.Windows.Forms.Button();
            this.btnDeleteVariable = new System.Windows.Forms.Button();
            this.btnEditVariable = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.cbDomain = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rbDeriveRequest = new System.Windows.Forms.RadioButton();
            this.rbRequested = new System.Windows.Forms.RadioButton();
            this.rbDerivable = new System.Windows.Forms.RadioButton();
            this.tbQuestion = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVariable)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvVariable
            // 
            this.dgvVariable.AllowUserToAddRows = false;
            this.dgvVariable.AllowUserToDeleteRows = false;
            this.dgvVariable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgvVariable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVariable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.VariableName});
            this.dgvVariable.Location = new System.Drawing.Point(12, 12);
            this.dgvVariable.MultiSelect = false;
            this.dgvVariable.Name = "dgvVariable";
            this.dgvVariable.ReadOnly = true;
            this.dgvVariable.Size = new System.Drawing.Size(240, 369);
            this.dgvVariable.TabIndex = 0;
            this.dgvVariable.SelectionChanged += new System.EventHandler(this.dgvVariable_SelectionChanged);
            // 
            // VariableName
            // 
            this.VariableName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.VariableName.DefaultCellStyle = dataGridViewCellStyle1;
            this.VariableName.HeaderText = "Переменная";
            this.VariableName.Name = "VariableName";
            this.VariableName.ReadOnly = true;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(299, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(178, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Свойства переменной";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.Location = new System.Drawing.Point(556, 349);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(44, 41);
            this.btnCancel.TabIndex = 42;
            this.toolTip1.SetToolTip(this.btnCancel, "Закрыть");
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnAddVariable
            // 
            this.btnAddVariable.Image = global::ExpertSystemShell.Properties.Resources.if_add_1342242;
            this.btnAddVariable.Location = new System.Drawing.Point(258, 33);
            this.btnAddVariable.Name = "btnAddVariable";
            this.btnAddVariable.Size = new System.Drawing.Size(28, 28);
            this.btnAddVariable.TabIndex = 30;
            this.toolTip1.SetToolTip(this.btnAddVariable, "Добавить переменную");
            this.btnAddVariable.UseVisualStyleBackColor = true;
            this.btnAddVariable.Click += new System.EventHandler(this.btnAddVariable_Click);
            // 
            // btnDeleteVariable
            // 
            this.btnDeleteVariable.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteVariable.Image")));
            this.btnDeleteVariable.Location = new System.Drawing.Point(258, 105);
            this.btnDeleteVariable.Name = "btnDeleteVariable";
            this.btnDeleteVariable.Size = new System.Drawing.Size(28, 31);
            this.btnDeleteVariable.TabIndex = 14;
            this.toolTip1.SetToolTip(this.btnDeleteVariable, "Удалить переменную");
            this.btnDeleteVariable.UseVisualStyleBackColor = true;
            this.btnDeleteVariable.Click += new System.EventHandler(this.btnDeleteVariable_Click);
            // 
            // btnEditVariable
            // 
            this.btnEditVariable.Image = ((System.Drawing.Image)(resources.GetObject("btnEditVariable.Image")));
            this.btnEditVariable.Location = new System.Drawing.Point(258, 67);
            this.btnEditVariable.Name = "btnEditVariable";
            this.btnEditVariable.Size = new System.Drawing.Size(28, 31);
            this.btnEditVariable.TabIndex = 13;
            this.toolTip1.SetToolTip(this.btnEditVariable, "Редактировать название переменной");
            this.btnEditVariable.UseVisualStyleBackColor = true;
            this.btnEditVariable.Click += new System.EventHandler(this.btnEditVariable_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(300, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 16);
            this.label3.TabIndex = 23;
            this.label3.Text = "Домен";
            // 
            // cbDomain
            // 
            this.cbDomain.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbDomain.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDomain.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbDomain.FormattingEnabled = true;
            this.cbDomain.Location = new System.Drawing.Point(303, 58);
            this.cbDomain.Name = "cbDomain";
            this.cbDomain.Size = new System.Drawing.Size(297, 24);
            this.cbDomain.TabIndex = 24;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(300, 85);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(117, 16);
            this.label4.TabIndex = 26;
            this.label4.Text = "Тип переменной";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.rbDeriveRequest);
            this.panel1.Controls.Add(this.rbRequested);
            this.panel1.Controls.Add(this.rbDerivable);
            this.panel1.Location = new System.Drawing.Point(303, 104);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(297, 88);
            this.panel1.TabIndex = 27;
            // 
            // rbDeriveRequest
            // 
            this.rbDeriveRequest.AutoSize = true;
            this.rbDeriveRequest.Location = new System.Drawing.Point(13, 59);
            this.rbDeriveRequest.Name = "rbDeriveRequest";
            this.rbDeriveRequest.Size = new System.Drawing.Size(163, 17);
            this.rbDeriveRequest.TabIndex = 2;
            this.rbDeriveRequest.TabStop = true;
            this.rbDeriveRequest.Text = "Выводимо-запрашиваемая";
            this.rbDeriveRequest.UseVisualStyleBackColor = true;
            // 
            // rbRequested
            // 
            this.rbRequested.AutoSize = true;
            this.rbRequested.Location = new System.Drawing.Point(13, 36);
            this.rbRequested.Name = "rbRequested";
            this.rbRequested.Size = new System.Drawing.Size(108, 17);
            this.rbRequested.TabIndex = 1;
            this.rbRequested.TabStop = true;
            this.rbRequested.Text = "Запрашиваемая";
            this.rbRequested.UseVisualStyleBackColor = true;
            // 
            // rbDerivable
            // 
            this.rbDerivable.AutoSize = true;
            this.rbDerivable.Location = new System.Drawing.Point(13, 13);
            this.rbDerivable.Name = "rbDerivable";
            this.rbDerivable.Size = new System.Drawing.Size(84, 17);
            this.rbDerivable.TabIndex = 0;
            this.rbDerivable.TabStop = true;
            this.rbDerivable.Text = "Выводимая";
            this.rbDerivable.UseVisualStyleBackColor = true;
            // 
            // tbQuestion
            // 
            this.tbQuestion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tbQuestion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbQuestion.Location = new System.Drawing.Point(303, 215);
            this.tbQuestion.Multiline = true;
            this.tbQuestion.Name = "tbQuestion";
            this.tbQuestion.Size = new System.Drawing.Size(297, 109);
            this.tbQuestion.TabIndex = 28;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(300, 196);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 16);
            this.label5.TabIndex = 29;
            this.label5.Text = "Текст вопроса";
            // 
            // VariablesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(612, 393);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnAddVariable);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbQuestion);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cbDomain);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnDeleteVariable);
            this.Controls.Add(this.btnEditVariable);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvVariable);
            this.Name = "VariablesForm";
            this.Text = "Переменные";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VariablesForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dgvVariable)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvVariable;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnDeleteVariable;
        private System.Windows.Forms.Button btnEditVariable;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbDomain;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rbDerivable;
        private System.Windows.Forms.RadioButton rbDeriveRequest;
        private System.Windows.Forms.RadioButton rbRequested;
        private System.Windows.Forms.TextBox tbQuestion;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridViewTextBoxColumn VariableName;
        private System.Windows.Forms.Button btnAddVariable;
        private System.Windows.Forms.Button btnCancel;
    }
}