﻿namespace ExpertSystemShell
{
    partial class RuleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RuleForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label2 = new System.Windows.Forms.Label();
            this.tbNewRule = new System.Windows.Forms.TextBox();
            this.btnEdit = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnAddPremiseVariable = new System.Windows.Forms.Button();
            this.btnAddPremise = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbPremiseValue = new System.Windows.Forms.ComboBox();
            this.cbPremiseVariable = new System.Windows.Forms.ComboBox();
            this.btnDeletePremise = new System.Windows.Forms.Button();
            this.dgvPremises = new System.Windows.Forms.DataGridView();
            this.Premise = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnDeleteConclusion = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnAddConclusionVariable = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnAddConclusion = new System.Windows.Forms.Button();
            this.dgvConclusion = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label5 = new System.Windows.Forms.Label();
            this.cbConclusionVariable = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbConclusionValue = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbExplaining = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPremises)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConclusion)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(133, 16);
            this.label2.TabIndex = 24;
            this.label2.Text = "Название правила";
            // 
            // tbNewRule
            // 
            this.tbNewRule.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbNewRule.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbNewRule.Location = new System.Drawing.Point(12, 28);
            this.tbNewRule.Name = "tbNewRule";
            this.tbNewRule.Size = new System.Drawing.Size(233, 26);
            this.tbNewRule.TabIndex = 23;
            // 
            // btnEdit
            // 
            this.btnEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnEdit.Location = new System.Drawing.Point(254, 33);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(89, 23);
            this.btnEdit.TabIndex = 25;
            this.btnEdit.Text = "Изменить";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnAddPremiseVariable);
            this.groupBox1.Controls.Add(this.btnAddPremise);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cbPremiseValue);
            this.groupBox1.Controls.Add(this.cbPremiseVariable);
            this.groupBox1.Controls.Add(this.btnDeletePremise);
            this.groupBox1.Controls.Add(this.dgvPremises);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(12, 62);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(366, 262);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Посылки";
            // 
            // btnAddPremiseVariable
            // 
            this.btnAddPremiseVariable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddPremiseVariable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnAddPremiseVariable.Location = new System.Drawing.Point(142, 199);
            this.btnAddPremiseVariable.Name = "btnAddPremiseVariable";
            this.btnAddPremiseVariable.Size = new System.Drawing.Size(37, 24);
            this.btnAddPremiseVariable.TabIndex = 32;
            this.btnAddPremiseVariable.Text = "...";
            this.toolTip1.SetToolTip(this.btnAddPremiseVariable, "Открыть форму добавления/редактирования переменных");
            this.btnAddPremiseVariable.UseVisualStyleBackColor = true;
            this.btnAddPremiseVariable.Click += new System.EventHandler(this.btnAddPremiseVariable_Click);
            // 
            // btnAddPremise
            // 
            this.btnAddPremise.Location = new System.Drawing.Point(6, 230);
            this.btnAddPremise.Name = "btnAddPremise";
            this.btnAddPremise.Size = new System.Drawing.Size(341, 23);
            this.btnAddPremise.TabIndex = 31;
            this.btnAddPremise.Text = "Добавить";
            this.btnAddPremise.UseVisualStyleBackColor = true;
            this.btnAddPremise.Click += new System.EventHandler(this.btnAddPremise_Click);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(207, 180);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 16);
            this.label4.TabIndex = 29;
            this.label4.Text = "Значение";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(6, 180);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 16);
            this.label3.TabIndex = 28;
            this.label3.Text = "Переменная";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(185, 199);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(19, 20);
            this.label1.TabIndex = 27;
            this.label1.Text = "=";
            // 
            // cbPremiseValue
            // 
            this.cbPremiseValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbPremiseValue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPremiseValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbPremiseValue.FormattingEnabled = true;
            this.cbPremiseValue.Location = new System.Drawing.Point(210, 199);
            this.cbPremiseValue.Name = "cbPremiseValue";
            this.cbPremiseValue.Size = new System.Drawing.Size(137, 24);
            this.cbPremiseValue.TabIndex = 26;
            // 
            // cbPremiseVariable
            // 
            this.cbPremiseVariable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbPremiseVariable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPremiseVariable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbPremiseVariable.FormattingEnabled = true;
            this.cbPremiseVariable.Location = new System.Drawing.Point(7, 199);
            this.cbPremiseVariable.Name = "cbPremiseVariable";
            this.cbPremiseVariable.Size = new System.Drawing.Size(129, 24);
            this.cbPremiseVariable.TabIndex = 25;
            this.cbPremiseVariable.SelectedIndexChanged += new System.EventHandler(this.cbPremiseVariable_SelectedIndexChanged);
            // 
            // btnDeletePremise
            // 
            this.btnDeletePremise.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDeletePremise.Image = ((System.Drawing.Image)(resources.GetObject("btnDeletePremise.Image")));
            this.btnDeletePremise.Location = new System.Drawing.Point(319, 22);
            this.btnDeletePremise.Name = "btnDeletePremise";
            this.btnDeletePremise.Size = new System.Drawing.Size(28, 32);
            this.btnDeletePremise.TabIndex = 15;
            this.toolTip1.SetToolTip(this.btnDeletePremise, "Удалить посылку");
            this.btnDeletePremise.UseVisualStyleBackColor = true;
            this.btnDeletePremise.Click += new System.EventHandler(this.btnDeletePremise_Click);
            // 
            // dgvPremises
            // 
            this.dgvPremises.AllowUserToAddRows = false;
            this.dgvPremises.AllowUserToDeleteRows = false;
            this.dgvPremises.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPremises.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Premise});
            this.dgvPremises.Location = new System.Drawing.Point(7, 22);
            this.dgvPremises.MultiSelect = false;
            this.dgvPremises.Name = "dgvPremises";
            this.dgvPremises.ReadOnly = true;
            this.dgvPremises.Size = new System.Drawing.Size(306, 150);
            this.dgvPremises.TabIndex = 0;
            // 
            // Premise
            // 
            this.Premise.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Premise.DefaultCellStyle = dataGridViewCellStyle1;
            this.Premise.HeaderText = "Посылка";
            this.Premise.Name = "Premise";
            this.Premise.ReadOnly = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.Location = new System.Drawing.Point(728, 347);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(44, 41);
            this.btnCancel.TabIndex = 41;
            this.toolTip1.SetToolTip(this.btnCancel, "Закрыть");
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnDeleteConclusion
            // 
            this.btnDeleteConclusion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDeleteConclusion.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteConclusion.Image")));
            this.btnDeleteConclusion.Location = new System.Drawing.Point(328, 21);
            this.btnDeleteConclusion.Name = "btnDeleteConclusion";
            this.btnDeleteConclusion.Size = new System.Drawing.Size(28, 33);
            this.btnDeleteConclusion.TabIndex = 33;
            this.toolTip1.SetToolTip(this.btnDeleteConclusion, "Удалить заключение");
            this.btnDeleteConclusion.UseVisualStyleBackColor = true;
            this.btnDeleteConclusion.Click += new System.EventHandler(this.btnDeleteConclusion_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(723, 13);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(44, 41);
            this.btnSave.TabIndex = 42;
            this.toolTip1.SetToolTip(this.btnSave, "Сохранить");
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnAddConclusionVariable
            // 
            this.btnAddConclusionVariable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddConclusionVariable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnAddConclusionVariable.Location = new System.Drawing.Point(159, 199);
            this.btnAddConclusionVariable.Name = "btnAddConclusionVariable";
            this.btnAddConclusionVariable.Size = new System.Drawing.Size(37, 24);
            this.btnAddConclusionVariable.TabIndex = 40;
            this.btnAddConclusionVariable.Text = "...";
            this.toolTip1.SetToolTip(this.btnAddConclusionVariable, "Открыть форму добавления/редактирования переменных");
            this.btnAddConclusionVariable.UseVisualStyleBackColor = true;
            this.btnAddConclusionVariable.Click += new System.EventHandler(this.btnAddConclusionVariable_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnAddConclusionVariable);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.btnAddConclusion);
            this.groupBox2.Controls.Add(this.dgvConclusion);
            this.groupBox2.Controls.Add(this.btnDeleteConclusion);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.cbConclusionVariable);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.cbConclusionValue);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox2.Location = new System.Drawing.Point(395, 62);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(372, 262);
            this.groupBox2.TabIndex = 27;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Заключения";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(230, 180);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 16);
            this.label7.TabIndex = 32;
            this.label7.Text = "Значение";
            // 
            // btnAddConclusion
            // 
            this.btnAddConclusion.Location = new System.Drawing.Point(15, 229);
            this.btnAddConclusion.Name = "btnAddConclusion";
            this.btnAddConclusion.Size = new System.Drawing.Size(341, 23);
            this.btnAddConclusion.TabIndex = 39;
            this.btnAddConclusion.Text = "Добавить";
            this.btnAddConclusion.UseVisualStyleBackColor = true;
            this.btnAddConclusion.Click += new System.EventHandler(this.btnAddConclusion_Click);
            // 
            // dgvConclusion
            // 
            this.dgvConclusion.AllowUserToAddRows = false;
            this.dgvConclusion.AllowUserToDeleteRows = false;
            this.dgvConclusion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvConclusion.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1});
            this.dgvConclusion.Location = new System.Drawing.Point(16, 21);
            this.dgvConclusion.MultiSelect = false;
            this.dgvConclusion.Name = "dgvConclusion";
            this.dgvConclusion.ReadOnly = true;
            this.dgvConclusion.Size = new System.Drawing.Size(306, 150);
            this.dgvConclusion.TabIndex = 32;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewTextBoxColumn1.HeaderText = "Заключение";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(13, 180);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 16);
            this.label5.TabIndex = 37;
            this.label5.Text = "Переменная";
            // 
            // cbConclusionVariable
            // 
            this.cbConclusionVariable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbConclusionVariable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbConclusionVariable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbConclusionVariable.FormattingEnabled = true;
            this.cbConclusionVariable.Location = new System.Drawing.Point(16, 199);
            this.cbConclusionVariable.Name = "cbConclusionVariable";
            this.cbConclusionVariable.Size = new System.Drawing.Size(137, 24);
            this.cbConclusionVariable.TabIndex = 34;
            this.cbConclusionVariable.SelectedIndexChanged += new System.EventHandler(this.cbConclusionVariable_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(194, 200);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(19, 20);
            this.label6.TabIndex = 36;
            this.label6.Text = "=";
            // 
            // cbConclusionValue
            // 
            this.cbConclusionValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbConclusionValue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbConclusionValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbConclusionValue.FormattingEnabled = true;
            this.cbConclusionValue.Location = new System.Drawing.Point(219, 198);
            this.cbConclusionValue.Name = "cbConclusionValue";
            this.cbConclusionValue.Size = new System.Drawing.Size(137, 24);
            this.cbConclusionValue.TabIndex = 35;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(15, 328);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 16);
            this.label8.TabIndex = 31;
            this.label8.Text = "Объяснение";
            // 
            // tbExplaining
            // 
            this.tbExplaining.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbExplaining.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbExplaining.Location = new System.Drawing.Point(20, 347);
            this.tbExplaining.Multiline = true;
            this.tbExplaining.Name = "tbExplaining";
            this.tbExplaining.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbExplaining.Size = new System.Drawing.Size(324, 36);
            this.tbExplaining.TabIndex = 30;
            this.tbExplaining.TextChanged += new System.EventHandler(this.tbExplaining_TextChanged);
            // 
            // RuleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 395);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tbExplaining);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbNewRule);
            this.Controls.Add(this.groupBox1);
            this.Name = "RuleForm";
            this.Text = "Добавление правила";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RuleForm_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPremises)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConclusion)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbNewRule;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgvPremises;
        private System.Windows.Forms.Button btnDeletePremise;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbPremiseValue;
        private System.Windows.Forms.ComboBox cbPremiseVariable;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnAddPremise;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnAddConclusion;
        private System.Windows.Forms.DataGridView dgvConclusion;
        private System.Windows.Forms.Button btnDeleteConclusion;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbConclusionVariable;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbConclusionValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbExplaining;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.DataGridViewTextBoxColumn Premise;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnAddPremiseVariable;
        private System.Windows.Forms.Button btnAddConclusionVariable;
    }
}