﻿namespace ExpertSystemShell
{
    partial class DomainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DomainForm));
            this.dgvDomain = new System.Windows.Forms.DataGridView();
            this.Domain = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvValues = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbNewDomain = new System.Windows.Forms.TextBox();
            this.tbNewValue = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnAddValue = new System.Windows.Forms.Button();
            this.btnDeleteValue = new System.Windows.Forms.Button();
            this.btnEditValue = new System.Windows.Forms.Button();
            this.btnDeleteDomain = new System.Windows.Forms.Button();
            this.btnEditDomain = new System.Windows.Forms.Button();
            this.btnAddDomain = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDomain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvValues)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvDomain
            // 
            this.dgvDomain.AllowDrop = true;
            this.dgvDomain.AllowUserToAddRows = false;
            this.dgvDomain.AllowUserToDeleteRows = false;
            this.dgvDomain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgvDomain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDomain.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Domain});
            this.dgvDomain.Location = new System.Drawing.Point(12, 12);
            this.dgvDomain.MultiSelect = false;
            this.dgvDomain.Name = "dgvDomain";
            this.dgvDomain.ReadOnly = true;
            this.dgvDomain.Size = new System.Drawing.Size(240, 347);
            this.dgvDomain.TabIndex = 0;
            this.dgvDomain.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDomain_CellEndEdit);
            this.dgvDomain.SelectionChanged += new System.EventHandler(this.dgvDomain_SelectionChanged);
            // 
            // Domain
            // 
            this.Domain.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Domain.DefaultCellStyle = dataGridViewCellStyle1;
            this.Domain.HeaderText = "Домен";
            this.Domain.Name = "Domain";
            this.Domain.ReadOnly = true;
            // 
            // dgvValues
            // 
            this.dgvValues.AllowUserToAddRows = false;
            this.dgvValues.AllowUserToDeleteRows = false;
            this.dgvValues.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvValues.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvValues.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1});
            this.dgvValues.Location = new System.Drawing.Point(320, 12);
            this.dgvValues.MultiSelect = false;
            this.dgvValues.Name = "dgvValues";
            this.dgvValues.ReadOnly = true;
            this.dgvValues.Size = new System.Drawing.Size(240, 347);
            this.dgvValues.TabIndex = 1;
            this.dgvValues.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvValues_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewTextBoxColumn1.HeaderText = "Значение";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // tbNewDomain
            // 
            this.tbNewDomain.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbNewDomain.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbNewDomain.Location = new System.Drawing.Point(13, 387);
            this.tbNewDomain.Name = "tbNewDomain";
            this.tbNewDomain.Size = new System.Drawing.Size(204, 26);
            this.tbNewDomain.TabIndex = 16;
            this.tbNewDomain.TextChanged += new System.EventHandler(this.tbNewDomain_TextChanged);
            this.tbNewDomain.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbNewDomain_KeyDown);
            // 
            // tbNewValue
            // 
            this.tbNewValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tbNewValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbNewValue.Location = new System.Drawing.Point(320, 389);
            this.tbNewValue.Name = "tbNewValue";
            this.tbNewValue.Size = new System.Drawing.Size(204, 26);
            this.tbNewValue.TabIndex = 18;
            this.tbNewValue.TextChanged += new System.EventHandler(this.tbNewValue_TextChanged);
            this.tbNewValue.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbNewValue_KeyDown);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(13, 366);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 20);
            this.label1.TabIndex = 19;
            this.label1.Text = "Новый домен";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(316, 366);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(194, 20);
            this.label2.TabIndex = 20;
            this.label2.Text = "Новое значение домена";
            // 
            // btnAddValue
            // 
            this.btnAddValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddValue.Image = global::ExpertSystemShell.Properties.Resources.if_add_1342242;
            this.btnAddValue.Location = new System.Drawing.Point(532, 389);
            this.btnAddValue.Name = "btnAddValue";
            this.btnAddValue.Size = new System.Drawing.Size(28, 28);
            this.btnAddValue.TabIndex = 17;
            this.toolTip1.SetToolTip(this.btnAddValue, "Добавить значение");
            this.btnAddValue.UseVisualStyleBackColor = true;
            this.btnAddValue.Click += new System.EventHandler(this.btnAddValue_Click);
            // 
            // btnDeleteValue
            // 
            this.btnDeleteValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteValue.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteValue.Image")));
            this.btnDeleteValue.Location = new System.Drawing.Point(566, 78);
            this.btnDeleteValue.Name = "btnDeleteValue";
            this.btnDeleteValue.Size = new System.Drawing.Size(28, 31);
            this.btnDeleteValue.TabIndex = 14;
            this.toolTip1.SetToolTip(this.btnDeleteValue, "Удалить значение");
            this.btnDeleteValue.UseVisualStyleBackColor = true;
            this.btnDeleteValue.Click += new System.EventHandler(this.btnDeleteValue_Click);
            // 
            // btnEditValue
            // 
            this.btnEditValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditValue.Image = ((System.Drawing.Image)(resources.GetObject("btnEditValue.Image")));
            this.btnEditValue.Location = new System.Drawing.Point(566, 32);
            this.btnEditValue.Name = "btnEditValue";
            this.btnEditValue.Size = new System.Drawing.Size(28, 31);
            this.btnEditValue.TabIndex = 13;
            this.toolTip1.SetToolTip(this.btnEditValue, "Редактировать значение");
            this.btnEditValue.UseVisualStyleBackColor = true;
            this.btnEditValue.Click += new System.EventHandler(this.btnEditValue_Click);
            // 
            // btnDeleteDomain
            // 
            this.btnDeleteDomain.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteDomain.Image")));
            this.btnDeleteDomain.Location = new System.Drawing.Point(258, 78);
            this.btnDeleteDomain.Name = "btnDeleteDomain";
            this.btnDeleteDomain.Size = new System.Drawing.Size(28, 31);
            this.btnDeleteDomain.TabIndex = 12;
            this.toolTip1.SetToolTip(this.btnDeleteDomain, "Удалить домен");
            this.btnDeleteDomain.UseVisualStyleBackColor = true;
            this.btnDeleteDomain.Click += new System.EventHandler(this.btnDeleteDomain_Click);
            // 
            // btnEditDomain
            // 
            this.btnEditDomain.Image = ((System.Drawing.Image)(resources.GetObject("btnEditDomain.Image")));
            this.btnEditDomain.Location = new System.Drawing.Point(258, 32);
            this.btnEditDomain.Name = "btnEditDomain";
            this.btnEditDomain.Size = new System.Drawing.Size(28, 31);
            this.btnEditDomain.TabIndex = 11;
            this.toolTip1.SetToolTip(this.btnEditDomain, "Редактировать домен");
            this.btnEditDomain.UseVisualStyleBackColor = true;
            this.btnEditDomain.Click += new System.EventHandler(this.btnEditDomain_Click);
            // 
            // btnAddDomain
            // 
            this.btnAddDomain.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAddDomain.Image = global::ExpertSystemShell.Properties.Resources.if_add_1342242;
            this.btnAddDomain.Location = new System.Drawing.Point(225, 387);
            this.btnAddDomain.Name = "btnAddDomain";
            this.btnAddDomain.Size = new System.Drawing.Size(28, 28);
            this.btnAddDomain.TabIndex = 10;
            this.toolTip1.SetToolTip(this.btnAddDomain, "Добавить домен");
            this.btnAddDomain.UseVisualStyleBackColor = true;
            this.btnAddDomain.Click += new System.EventHandler(this.btnAddDomain_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.Location = new System.Drawing.Point(566, 389);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(44, 41);
            this.btnCancel.TabIndex = 42;
            this.toolTip1.SetToolTip(this.btnCancel, "Закрыть");
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // DomainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(617, 439);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbNewValue);
            this.Controls.Add(this.btnAddValue);
            this.Controls.Add(this.tbNewDomain);
            this.Controls.Add(this.btnDeleteValue);
            this.Controls.Add(this.btnEditValue);
            this.Controls.Add(this.btnDeleteDomain);
            this.Controls.Add(this.btnEditDomain);
            this.Controls.Add(this.btnAddDomain);
            this.Controls.Add(this.dgvValues);
            this.Controls.Add(this.dgvDomain);
            this.Name = "DomainForm";
            this.Text = "Редатирование доменов";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DomainForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDomain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvValues)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvDomain;
        private System.Windows.Forms.DataGridViewTextBoxColumn Domain;
        private System.Windows.Forms.DataGridView dgvValues;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.Button btnDeleteDomain;
        private System.Windows.Forms.Button btnAddDomain;
        private System.Windows.Forms.Button btnDeleteValue;
        private System.Windows.Forms.Button btnEditValue;
        private System.Windows.Forms.TextBox tbNewDomain;
        private System.Windows.Forms.TextBox tbNewValue;
        private System.Windows.Forms.Button btnAddValue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btnEditDomain;
        private System.Windows.Forms.Button btnCancel;
    }
}