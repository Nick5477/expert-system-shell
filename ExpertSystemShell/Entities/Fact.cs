﻿namespace ExpertSystemShell.Entities
{
    using System;

    /// <summary>
    /// Факт.
    /// </summary>
    public class Fact
    {
        public Variable Variable { get; set; }

        public DomainValue Value { get; set; }

        public int Order { get; set; }

        public string PremiseContent => $"{Variable.Name} == {Value.Value}";

        public string ConclusionContent => $"{Variable.Name} = {Value.Value}";

        public Fact()
        {
            
        }

        public Fact(Variable variable, DomainValue value)
        {
            if (variable.Domain.GetValue(value.Value) == null)
            {
                throw new ArgumentNullException($"Значение {value.Value} отсутствует в домене");
            }
            Variable = variable;
            Value = value;
        }
    }
}
