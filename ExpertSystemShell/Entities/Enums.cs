﻿namespace ExpertSystemShell.Entities
{
    public enum EFormTypeEnum
    {
        Add = 1,
        Edit = 2,
    }

    public enum EFormAnswer
    {
        Ok = 1,
        No = 2,
        MoveToPrev = 3
    }
}
