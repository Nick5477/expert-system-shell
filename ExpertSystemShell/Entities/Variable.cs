﻿namespace ExpertSystemShell.Entities
{
    using System.Linq;

    /// <summary>
    /// Тип переменной.
    /// </summary>
    public enum tVariableType
    {
        /// <summary>
        /// Выводимая.
        /// </summary>
        Derivable,

        /// <summary>
        /// Запрашиваемая.
        /// </summary>
        Requested,

        /// <summary>
        /// Выводимо-запрашиваемая.
        /// </summary>
        DerivableRequested
    }

    /// <summary>
    /// Переменная.
    /// </summary>
    public class Variable
    {
        public string Name { get; set; }

        public Domain Domain { get; set; }

        public tVariableType Type { get; set; }

        public string Question { get; set; }

        public Variable Original { get; set; }

        public bool IsRemoved { get; set; }

        public Variable(string name)
        {
            Name = name;
            Domain = new Domain();
        }

        public Variable()
        {
            Domain = new Domain();
        }

        public Variable(string name, Domain domain, tVariableType type, string question, Variable original)
        {
            Name = name;
            Domain = domain;
            Type = type;
            Question = question;
            Original = original;
        }

        public DomainValue GetDomainValue(string value)
        {
            return Domain.Values.FirstOrDefault(v => v.Value == value);
        }
    }
}
