﻿namespace ExpertSystemShell.Entities
{
    /// <summary>
    /// Значение домена.
    /// </summary>
    public class DomainValue
    {
        public string Value { get; set; }

        public DomainValue Original { get; set; }

        public DomainValue()
        {
            
        }

        public DomainValue(string value)
        {
            Value = value;
        }

        public DomainValue(string value, DomainValue original)
        {
            Value = value;
            Original = original;
        }
    }
}
