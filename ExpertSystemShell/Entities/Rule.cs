﻿namespace ExpertSystemShell.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class Rule : ICloneable
    {
        public string Name { get; set; }

        public List<Fact> Premises { get; set; }

        public List<Fact> Conclusions { get; set; }

        public string Content => $"ЕСЛИ "
                                 + $"{string.Join(" && ", Premises.Select(p => $"{p.Variable.Name}=={p.Value.Value}"))}"
                                 + $", ТО "
                                 + $"{string.Join(" & ", Conclusions.Select(c => $"{c.Variable.Name}={c.Value.Value}"))}";

        public string Reason { get; set; }

        public int Order { get; set; }

        public Rule Original { get; set; }

        public Rule()
        {
            Premises = new List<Fact>();
            Conclusions = new List<Fact>();
        }

        public Rule(string name)
        {
            Name = name;
            Premises = new List<Fact>();
            Conclusions = new List<Fact>();
        }

        public Rule(string name, string reason, int position)
        {
            Premises = new List<Fact>();
            Conclusions = new List<Fact>();
            Name = name;
            Reason = reason;
            Order = position;
        }

        public Rule(Rule rule)
        {
            Premises = new List<Fact>(rule.Premises);
            Conclusions = new List<Fact>(rule.Conclusions);
            Name = rule.Name;
            Reason = rule.Reason;
            Order = rule.Order;
        }

        public void AddPremise(Fact fact)
        {
            Premises.Add(fact);
        }

        public void AddConclusion(Fact fact)
        {
            Conclusions.Add(fact);
        }

        public void DeletePremise(string variableName, string domainValue)
        {
            Premises.RemoveAll(
                p => p.Variable.Name == variableName
                     && p.Value.Value == domainValue);
        }

        public void DeleteConclusion(string variableName, string domainValue)
        {
            Conclusions.RemoveAll(
                c => c.Variable.Name == variableName
                     && c.Value.Value == domainValue);
        }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
