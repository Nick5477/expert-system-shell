﻿namespace ExpertSystemShell.Entities
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Windows.Forms;
    using System.Xml.Serialization;

    public class KnowledgeBase
    {
        private ConsultationForm _consultForm;

        public string Name { get; set; }

        public List<Domain> Domains { get; set; }

        public List<Rule> Rules { get; set; }

        public List<Variable> Variables { get; set; }

        public Variable Goal { get; set; }

        [XmlIgnore]
        public List<Rule> TriggeredRules { get; set; }

        [XmlIgnore]
        public Dictionary<string, DomainValue> WorkMemory { get; set; }

        /// <summary>
        /// Флаг продолжения консультации
        /// </summary>
        [XmlIgnore]
        public bool IsCountinue { get; set; }

        /// <summary>
        /// Список использованных переменных
        /// </summary>
        private Dictionary<string, bool> _variableState = new Dictionary<string, bool>();

        /// <summary>
        /// Последнее означимая переменная
        /// </summary>
        [XmlIgnore]
        private string _lastUsedVariable = string.Empty;

        /// <summary>
        /// Цепочка сработавших правил
        /// </summary>
        [XmlIgnore]
        public Dictionary<Rule, int> Chain { get; }

        public KnowledgeBase()
        {
            Domains = new List<Domain>();
            Rules = new List<Rule>();
            Variables = new List<Variable>();
            TriggeredRules = new List<Rule>();
            WorkMemory = new Dictionary<string, DomainValue>();
            Chain = new Dictionary<Rule, int>();
            IsCountinue = true;
        }

        #region Add

        public void AddDomain(Domain domain)
        {
            Domains.Add(domain);
        }

        public void AddRule(Rule rule)
        {
            Rules.RemoveAll(r => r == rule.Original);
            if (Rules.Any(r => r.Order == rule.Order))
            {
                Rules.ForEach(r => r.Order += rule.Order <= r.Order ? 1 : 0);
            }
            
            Rules.Add(rule);
        }

        public void AddVariable(Variable variable)
        {
            Variables.Add(variable);
        }

        public void PushRule(Rule rule)
        {
            TriggeredRules.Add(rule);
        }

        public void AddDomainValue(string domainName, string value)
        {
            Domains.FirstOrDefault(d => d.Name.ToUpper().Trim() == domainName?.ToUpper().Trim())?.AddValue(value);
        }

        #endregion

        #region Delete

        public void DeleteRule(string ruleName)
        {
            Rules.RemoveAll(r => r.Name.ToUpper().Trim() == ruleName?.ToUpper().Trim());
        }

        public void DeleteVariable(string variableName)
        {
            Variables.RemoveAll(r => r.Name.ToUpper().Trim() == variableName?.ToUpper().Trim());
        }

        public void DeleteDomain(string domainName)
        {
            Domains.RemoveAll(d => d.Name.ToUpper().Trim() == domainName?.ToUpper().Trim());
        }

        public void DeleteDomainValue(string domainName, string value)
        {
            Domains.FirstOrDefault(d => d.Name.ToUpper().Trim() == domainName?.ToUpper().Trim())?.DeleteValue(value);
        }

        #endregion

        #region GET

        public Rule GetRule(string ruleName)
        {
            return Rules.FirstOrDefault(r => r.Name.ToUpper().Trim() == ruleName?.ToUpper().Trim());
        }

        public Variable GetVariable(string variableName)
        {
            return Variables.FirstOrDefault(v => v.Name.ToUpper().Trim() == variableName?.ToUpper().Trim());
        }

        public Domain GetDomain(string domainName)
        {
            return Domains.FirstOrDefault(d => d.Name.ToUpper().Trim() == domainName?.ToUpper().Trim());
        }

        public Rule GetRuleByDomain(string domainName)
        {
            return Rules.FirstOrDefault(r => r.Conclusions.Any(c => c.Variable.Domain.Name == domainName) 
                                             || r.Premises.Any(p => p.Variable.Domain.Name == domainName));
        }

        public Rule GetRuleByVariable(string variableName)
        {
            return Rules.FirstOrDefault(r => r.Conclusions.Any(c => c.Variable.Name == variableName)
                                             || r.Premises.Any(p => p.Variable.Name == variableName));
        }

        #endregion

        #region Replace

        public void ReplaceDomains(List<Domain> domains)
        {
            foreach (var domain in domains)
            {
                if (domain.Original != null)
                {
                    Domains.Remove(domain.Original);                    
                }

                if (!domain.IsRemoved)
                {
                    Domains.Add(domain);

                    foreach (var value in domain.Values)
                    {
                        var premiseList = Rules.Select(r => r.Premises.Where(p => p.Value.Value == value.Original?.Value));
                        foreach (var premises in premiseList)
                        {
                            foreach (var premise in premises)
                            {
                                premise.Value = value;
                            }
                        }

                        var conclusionList = Rules.Select(r => r.Conclusions.Where(p => p.Value.Value == value.Original?.Value));
                        foreach (var conclusions in conclusionList)
                        {
                            foreach (var conclusion in conclusions)
                            {
                                conclusion.Value = value;
                            }
                        }
                    }

                    var variables = Variables.Where(v => v.Domain.Name == domain.Original?.Name);
                    foreach (var variable in variables)
                    {
                        variable.Domain = domain;
                    }

                }
                else
                {
                    Rules.RemoveAll(
                        r => r.Premises.Any(p => p.Variable.Domain.Name == domain.Original?.Name)
                             || r.Conclusions.Any(c => c.Variable.Domain.Name == domain.Original?.Name));
                    Variables.RemoveAll(v => v.Domain.Name == domain.Name);
                }
            }
        }

        public void ReplaceVariables(List<Variable> variables)
        {
            foreach (var variable in variables)
            {
                int oldVariableCount = Variables.Count;
                if (variable.Original != null)
                {
                    Variables.Remove(variable.Original);
                }

                if (!variable.IsRemoved)
                {
                    Variables.Add(variable);
                    var rulesPremise = Rules.Where(r => r.Premises.Any(p => p.Variable.Name == variable.Original?.Name));
                    var rulesConclusion = Rules.Where(r => r.Conclusions.Any(c => c.Variable.Name == variable.Original?.Name));
                    foreach (var rule in rulesPremise)
                    {
                        foreach (var premise in rule.Premises)
                        {
                            if (premise.Variable.Name == variable.Original?.Name)
                            {
                                premise.Variable = variable;
                            }
                        }
                    }

                    foreach (var rule in rulesConclusion)
                    {
                        foreach (var conclusion in rule.Conclusions)
                        {
                            if (conclusion.Variable.Name == variable.Original?.Name)
                            {
                                conclusion.Variable = variable;
                            }
                        }
                    }
                }
                else
                {
                    Rules.RemoveAll(
                        r => r.Premises.Any(p => p.Variable.Name == variable.Original?.Name)
                             || r.Conclusions.Any(c => c.Variable.Name == variable.Original?.Name));
                }
                
            }
        }

        #endregion

        #region Save/Load
        public void Save(string fileName)
        {
            var formatter = new XmlSerializer(typeof(KnowledgeBase),
                new[]{ typeof(KnowledgeBase), typeof(Domain), typeof(DomainValue), typeof(tVariableType),
                    typeof(Fact), typeof(Rule), typeof(Variable)});
            using (FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, this);
            }
        }

        public KnowledgeBase Load(string fileName)
        {
            var formatter = new XmlSerializer(typeof(KnowledgeBase),
                new[]{ typeof(KnowledgeBase), typeof(Domain), typeof(DomainValue), typeof(tVariableType),
                    typeof(Fact), typeof(Rule), typeof(Variable)});
            using (FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate))
            {
                KnowledgeBase knowledgeBase = (KnowledgeBase)formatter.Deserialize(fs);
                return knowledgeBase;
            }
        }
        #endregion

        #region Consultation

        /// <summary>
        /// Старт консультации
        /// </summary>
        public void Consult(Variable goal)
        {
            Goal = goal;
            var current = goal;//текущая цель консультации для рекурсии
            _consultForm = new ConsultationForm(current.Name, Name);
            WorkMemory.Clear();/* очищаю рабочую память */
            TriggeredRules.Clear();
            _variableState.Clear();
            Chain.Clear();
            foreach (var variable in Variables)
            {
                if (!_variableState.ContainsKey(variable.Name))
                {
                    _variableState.Add(variable.Name, false);
                }
                
            }
            IsCountinue = true;

            if (Work(current, 0))
            {
                if (WorkMemory.ContainsKey(current.Name))
                {
                    MessageBox.Show($"Цель консультации определилась! Результат - {Goal.Name} = {WorkMemory[current.Name].Value}", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show($"Цель консультации не определилась!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            _consultForm.Dispose();
        }

        /// <summary>
        /// Основная логика консультации
        /// </summary>
        private bool Work(Variable current, int number)
        {
            //если консультация не оборвана
            if (IsCountinue)
            {
                //если переменная запрашивамаемая и на вопрос не ответили (закрыли форму)
                if (current.Type == tVariableType.Requested && AskQuestion(current) != EFormAnswer.Ok)
                {
                    return false;
                }

                //иду по списку правил
                foreach (var rule in Rules.OrderBy(x => x.Order))
                {
                    //если правило уже использовано или в его заключении нет текущей подцели, то пропускаю
                    if (TriggeredRules.Contains(rule) || !rule.Conclusions.Any(x => x.Variable.Name == current.Name))
                    {
                        continue;
                    }
                    else
                    {
                        TriggeredRules.Add(rule);
                        //иначе обрабатываю посылки
                        var flag = true;
                        foreach (var fact in rule.Premises)
                        {
                            //если переменная в посылке еще не означена и проваливаемся внтурь.. вдруг вернулся нет
                            if (!_variableState[fact.Variable.Name] && !Work(fact.Variable, number + 1))
                            {
                                return false;
                            }
                            //если переменная еще не означена или значение значение в памяти, не совпадает с тем, что в поссылке
                            if (!_variableState[fact.Variable.Name] || WorkMemory[fact.Variable.Name].Value != fact.Value.Value)
                            {
                                flag = false;
                                break;
                            }
                        }
                        //если прошли все предпоссылки и оказалось, что все ок
                        if (flag)
                        {
                            //все заключение пишем в рабочую память
                            foreach (var fact in rule.Conclusions)
                            {
                                _variableState[fact.Variable.Name] = true;
                                WorkMemory[fact.Variable.Name] = fact.Value;
                            }
                            Chain.Add(rule, number);//добавляем правило в сработавшую цепочку правил для вывода дерева
                        }
                    }
                }
                if (current.Type == tVariableType.DerivableRequested)
                {
                    /* есть вероятность, что она уже означена ранее */
                    if (!WorkMemory.ContainsKey(current.Name) || !_variableState[current.Name])
                    {
                        AskQuestion(current);
                    }
                }
                return true;
            }
            return false;
        }

        /// <summary>
        /// Логика задавания вопроса
        /// </summary>
        /// <param name="variable"></param>
        /// <returns></returns>
        private EFormAnswer AskQuestion(Variable variable)
        {
            /* вызываю вопросную форму */
            _consultForm.AskQuestion(variable);
            _consultForm.StartPosition = FormStartPosition.CenterScreen;
            var result = _consultForm.ShowDialog();
            //если нажали ОК, обычная обработка
            if (result == DialogResult.OK)
            {
                _variableState[variable.Name] = true;
                WorkMemory[variable.Name] = _consultForm.Answer;
                _lastUsedVariable = variable.Name;
            }
            //если нажали вернуться к предыдущему
            if (result == DialogResult.Cancel)
            {
                _variableState[_lastUsedVariable] = false;
                WorkMemory.Remove(_lastUsedVariable);
            }
            //если нажали закончить консультацию
            if (result == DialogResult.Abort)
            {
                IsCountinue = false;
            }
            switch (result)
            {
                case DialogResult.OK:
                    {
                        return EFormAnswer.Ok;
                    }
                case DialogResult.Cancel:
                    {
                        return EFormAnswer.MoveToPrev;
                    }
                case DialogResult.Abort:
                    {
                        return EFormAnswer.No;
                    }
                default:
                    {
                        return EFormAnswer.No;
                    }
            }
        }

        #endregion

    }
}
