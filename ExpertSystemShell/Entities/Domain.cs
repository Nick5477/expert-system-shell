﻿namespace ExpertSystemShell.Entities
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Домен.
    /// </summary>
    public class Domain
    {
        public string Name { get; set; }

        public List<DomainValue> Values { get; set; }

        public Domain Original { get; set; }

        public bool IsRemoved { get; set; }

        public Domain()
        {
            Values = new List<DomainValue>();
        }

        public Domain(string name, List<DomainValue> values, Domain original)
        {
            Name = name;
            Values = new List<DomainValue>();
            foreach (var value in values)
            {
                Values.Add(new DomainValue(value.Value, value));
            }
            Original = original;
        }

        public Domain(string name)
        {
            Values = new List<DomainValue>();
            Name = name;
        }

        public void AddValue(string value)
        {
            Values.Add(new DomainValue(value));
        }

        public void DeleteValue(string value)
        {
            Values.RemoveAll(v => v.Value.ToUpper().Trim() == value.ToUpper().Trim());
        }

        public DomainValue GetValue(string value)
        {
            return Values.FirstOrDefault(v => v.Value.ToUpper().Trim() == value.ToUpper().Trim());
        }
    }
}
