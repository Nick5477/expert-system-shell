﻿namespace ExpertSystemShell
{
    using System;
    using System.Drawing;
    using System.Linq;
    using System.Windows.Forms;

    using ExpertSystemShell.Entities;

    public partial class MainForm : Form
    {
        private KnowledgeBase _knowledgeBase = new KnowledgeBase();

        private DomainForm _domainForm;

        private VariablesForm _variablesForm;

        private ConsultationGoalForm _consultationGoalForm;

        private ExplainingForm _explainingForm;

        private RuleForm _ruleForm;

        private Rectangle dragBoxFromMouseDown;

        private int rowIndexFromMouseDown;

        private int rowIndexOfItemUnderMouseToDrop;

        public MainForm()
        {
            InitializeComponent();
            DisableKnowledgeBaseControls();
            InitializeRules();
        }

        #region Drag and drop Premises

        private void dgvPremises_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
            {
                // If the mouse moves outside the rectangle, start the drag.
                if (dragBoxFromMouseDown != Rectangle.Empty &&
                    !dragBoxFromMouseDown.Contains(e.X, e.Y))
                {
                    // Proceed with the drag and drop, passing in the list item.
                    DragDropEffects dropEffect = dgvPremises.DoDragDrop(
                             dgvPremises.Rows[rowIndexFromMouseDown],
                             DragDropEffects.Move);
                }
            }
        }

        private void dgvPremises_MouseDown(object sender, MouseEventArgs e)
        {
            // Get the index of the item the mouse is below.
            rowIndexFromMouseDown = dgvPremises.HitTest(e.X, e.Y).RowIndex;
            if (rowIndexFromMouseDown != -1)
            {
                // Remember the point where the mouse down occurred. 
                // The DragSize indicates the size that the mouse can move 
                // before a drag event should be started.               
                Size dragSize = SystemInformation.DragSize;
                // Create a rectangle using the DragSize, with the mouse position being
                // at the center of the rectangle.
                dragBoxFromMouseDown = new Rectangle(new Point(e.X - (dragSize.Width / 2),
                                                               e.Y - (dragSize.Height / 2)),
                                                        dragSize);
            }
            else
                // Reset the rectangle if the mouse is not over an item in the ListBox.
                dragBoxFromMouseDown = Rectangle.Empty;
        }

        private void dgvPremises_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void dgvPremises_DragDrop(object sender, DragEventArgs e)
        {
            // The mouse locations are relative to the screen, so they must be
            // converted to client coordinates.
            Point clientPoint = dgvPremises.PointToClient(new Point(e.X, e.Y));

            // Get the row index of the item the mouse is below.
            rowIndexOfItemUnderMouseToDrop =
                dgvPremises.HitTest(clientPoint.X, clientPoint.Y).RowIndex;

            // If the drag operation was a move then remove and insert the row.
            if (e.Effect == DragDropEffects.Move)
            {
                DataGridViewRow rowToMove = e.Data.GetData(
                             typeof(DataGridViewRow)) as DataGridViewRow;

                if (rowIndexOfItemUnderMouseToDrop != -1)
                {
                    dgvPremises.Rows.RemoveAt(rowIndexFromMouseDown);
                    dgvPremises.Rows.Insert(rowIndexOfItemUnderMouseToDrop, rowToMove);
                    for (int i = 0; i < dgvPremises.RowCount; i++)
                    {
                        var premise = (Fact)dgvPremises.Rows[i].Tag;
                        premise.Order = i;
                    }
                }
            }
        }

        #endregion

        #region Drag and drop Rules

        private void dgvRules_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
            {
                // If the mouse moves outside the rectangle, start the drag.
                if (dragBoxFromMouseDown != Rectangle.Empty &&
                    !dragBoxFromMouseDown.Contains(e.X, e.Y))
                {
                    // Proceed with the drag and drop, passing in the list item.
                    DragDropEffects dropEffect = dgvRules.DoDragDrop(
                             dgvRules.Rows[rowIndexFromMouseDown],
                             DragDropEffects.Move);
                }
            }
        }

        private void dgvRules_MouseDown(object sender, MouseEventArgs e)
        {
            // Get the index of the item the mouse is below.
            rowIndexFromMouseDown = dgvRules.HitTest(e.X, e.Y).RowIndex;
            if (rowIndexFromMouseDown != -1)
            {
                // Remember the point where the mouse down occurred. 
                // The DragSize indicates the size that the mouse can move 
                // before a drag event should be started.               
                Size dragSize = SystemInformation.DragSize;
                // Create a rectangle using the DragSize, with the mouse position being
                // at the center of the rectangle.
                dragBoxFromMouseDown = new Rectangle(new Point(e.X - (dragSize.Width / 2),
                                                               e.Y - (dragSize.Height / 2)),
                                                        dragSize);
            }
            else
                // Reset the rectangle if the mouse is not over an item in the ListBox.
                dragBoxFromMouseDown = Rectangle.Empty;
        }

        private void dgvRules_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void dgvRules_DragDrop(object sender, DragEventArgs e)
        {
            // The mouse locations are relative to the screen, so they must be
            // converted to client coordinates.
            Point clientPoint = dgvRules.PointToClient(new Point(e.X, e.Y));

            // Get the row index of the item the mouse is below.
            rowIndexOfItemUnderMouseToDrop =
                dgvRules.HitTest(clientPoint.X, clientPoint.Y).RowIndex;

            // If the drag operation was a move then remove and insert the row.
            if (e.Effect == DragDropEffects.Move)
            {
                DataGridViewRow rowToMove = e.Data.GetData(
                             typeof(DataGridViewRow)) as DataGridViewRow;

                if (rowIndexOfItemUnderMouseToDrop != -1)
                {
                    dgvRules.Rows.RemoveAt(rowIndexFromMouseDown);
                    dgvRules.Rows.Insert(rowIndexOfItemUnderMouseToDrop, rowToMove);
                    for (int i = 0; i < dgvRules.RowCount; i++)
                    {
                        var rule = (Rule)dgvRules.Rows[i].Tag;
                        rule.Order = i;
                    }

                    dgvRules.CurrentCell = dgvRules.Rows[rowIndexOfItemUnderMouseToDrop].Cells[0];
                    var insertedRule = (Rule)dgvRules.Rows[rowIndexOfItemUnderMouseToDrop].Tag;
                    InitializeConclusions(insertedRule.Name);
                    InitializePremises(insertedRule.Name);
                }
            }
        }

        #endregion

        #region Init

        private void InitializeRules()
        {
            dgvRules.Rows.Clear();
            var rules = _knowledgeBase.Rules.OrderBy(x => x.Order).ToList();
            foreach (var rule in rules)
            {
                var row = new DataGridViewRow();
                row.CreateCells(dgvRules, rule.Name, rule.Content);
                row.Tag = rule;
                dgvRules.Rows.Add(row);
            }
            InitializePremises(rules.FirstOrDefault()?.Name);
            InitializeConclusions(rules.FirstOrDefault()?.Name);
        }

        private void InitializePremises(string ruleName)
        {
            dgvPremises.Rows.Clear();
            var rule = _knowledgeBase.GetRule(ruleName);
            if (rule != null)
            {
                var premises = rule.Premises.OrderBy(x => x.Order).ToList();
                foreach (var premise in premises)
                {
                    var row = new DataGridViewRow();
                    row.CreateCells(dgvPremises, premise.Variable.Name, premise.Value.Value);
                    row.Tag = premise;
                    dgvPremises.Rows.Add(row);
                }
            }
        }

        private void InitializeConclusions(string ruleName)
        {
            dgvConclusions.Rows.Clear();
            var rule = _knowledgeBase.GetRule(ruleName);
            if (rule != null)
            {
                foreach (var conclusion in rule.Conclusions)
                {
                    var row = new DataGridViewRow();
                    row.CreateCells(dgvConclusions, conclusion.Variable.Name, conclusion.Value.Value);
                    row.Tag = conclusion;
                    dgvConclusions.Rows.Add(row);
                }
            }
        }

        #endregion

        #region Меню База знаний

        private void редатироватьПеременныеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _variablesForm = new VariablesForm(_knowledgeBase);
            _variablesForm.ShowDialog();
            _variablesForm.Dispose();
            InitializeRules();
        }

        private void редактироватьДоменToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _domainForm = new DomainForm(_knowledgeBase);
            _domainForm.ShowDialog();
            _domainForm.Dispose();
            InitializeRules();
        }

        #endregion

        #region Disabling/enabling controls

        private void DisableKnowledgeBaseControls()
        {
            базаЗнанийToolStripMenuItem.Visible = false;
            консультацияToolStripMenuItem.Visible = false;
            объяснениеToolStripMenuItem.Visible = false;
            tbBaseName.Enabled = false;
            btnSubmitBaseName.Enabled = false;
            btnAdd.Enabled = false;
            btnEdit.Enabled = false;
            btnDelete.Enabled = false;
        }

        private void EnableKnowledgeBaseControls()
        {
            базаЗнанийToolStripMenuItem.Visible = true;
            консультацияToolStripMenuItem.Visible = true;
            объяснениеToolStripMenuItem.Visible = true;
            tbBaseName.Enabled = true;
            btnSubmitBaseName.Enabled = true;
            btnAdd.Enabled = true;
            btnEdit.Enabled = true;
            btnDelete.Enabled = true;
        }

        #endregion

        #region Click events

        private void btnSubmitBaseName_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tbBaseName.Text))
            {
                _knowledgeBase.Name = tbBaseName.Text;
                MessageBox.Show("Имя базы знаний изменено!");
            }
            else
            {
                MessageBox.Show("Имя базы знаний не должно быть пустым или состоять из одних пробелов!");
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int? index = dgvRules.CurrentCell?.RowIndex;
            int oldRulesCount = _knowledgeBase.Rules.Count;

            _ruleForm = new RuleForm(_knowledgeBase, dgvRules.CurrentCell?.RowIndex);
            _ruleForm.ShowDialog();
            InitializeRules();

            if (index.HasValue
                && oldRulesCount < _knowledgeBase.Rules.Count)
            {
                dgvRules.CurrentCell = dgvRules.Rows[index.Value+1].Cells[0];
            }
            
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            int? index = dgvRules.CurrentCell?.RowIndex;

            _ruleForm = new RuleForm(_knowledgeBase, (Rule)dgvRules.CurrentRow?.Tag);
            _ruleForm.ShowDialog();
            InitializeRules();

            if (index.HasValue)
            {
                dgvRules.CurrentCell = dgvRules.Rows[index.Value].Cells[0];
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteRule((Rule)dgvRules.CurrentRow?.Tag);
        }

        private void начатьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _consultationGoalForm = new ConsultationGoalForm(_knowledgeBase);
            _consultationGoalForm.ShowDialog();
            _consultationGoalForm.Dispose();
        }

        private void получитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _explainingForm = new ExplainingForm(_knowledgeBase);
            _explainingForm.ShowDialog();
            _explainingForm.Dispose();
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        #endregion

        #region DELETE

        private void DeleteRule(Rule rule)
        {
            if (rule != null)
            {
                _knowledgeBase.DeleteRule(rule.Name);
                dgvRules.Rows.Remove(dgvRules.CurrentRow);
            }
        }

        #endregion

        #region Меню файл

        private void сохранитьКакToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var fileDialog = new SaveFileDialog();
            fileDialog.InitialDirectory = Environment.CurrentDirectory;
            fileDialog.Filter = "XML Files (*.xml)|*.xml";
            fileDialog.DefaultExt = "xml";
            fileDialog.FilterIndex = 0;
            fileDialog.FileName = _knowledgeBase.Name;
            var dialog = fileDialog.ShowDialog();

            if (dialog == DialogResult.OK || dialog == DialogResult.Yes)
            {
                _knowledgeBase.Save(fileDialog.FileName);
            }
        }

        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var fileDialog = new OpenFileDialog();
            fileDialog.InitialDirectory = Environment.CurrentDirectory;
            fileDialog.Filter = "XML Files (*.xml)|*.xml";
            fileDialog.DefaultExt = "xml";
            fileDialog.FilterIndex = 0;
            var dialog = fileDialog.ShowDialog();

            if (dialog == DialogResult.OK || dialog == DialogResult.Yes)
            {
                _knowledgeBase = _knowledgeBase.Load(fileDialog.FileName);
                EnableKnowledgeBaseControls();
                tbBaseName.Text = _knowledgeBase.Name;
                InitializeRules();
            }
        }

        private void новыйToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _knowledgeBase = new KnowledgeBase()
            {
                Name = "newKnowledgeBase"
            };
            tbBaseName.Text = "newKnowledgeBase";
            EnableKnowledgeBaseControls();
        }

        #endregion

        #region Changed events

        private void dgvRules_SelectionChanged(object sender, EventArgs e)
        {
            var rule = (Rule)dgvRules.CurrentRow?.Tag;
            InitializePremises(rule?.Name);
            InitializeConclusions(rule?.Name);
        }


        #endregion


    }
}
