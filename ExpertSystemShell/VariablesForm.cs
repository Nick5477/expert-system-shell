﻿namespace ExpertSystemShell
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Forms;

    using ExpertSystemShell.Entities;

    public partial class VariablesForm : Form
    {
        private AddEditVariableForm _addEditVariableForm;

        private readonly KnowledgeBase _knowledgeBase;

        private List<Variable> _variables = new List<Variable>();

        private bool _isChanged = false;

        public Variable NewVariable { get; set; }

        public VariablesForm(KnowledgeBase knowledgeBase)
        {
            _knowledgeBase = knowledgeBase;
            InitVariableList();
            InitializeComponent();
            InitializeDomains();
            if (_knowledgeBase.Variables.Count != 0)
            {
                InitializeVariables();
                InitializeVariableProperties(_knowledgeBase.Variables.FirstOrDefault());
            }

            cbDomain.Enabled = false;
            rbDerivable.Enabled = false;
            rbDeriveRequest.Enabled = false;
            rbRequested.Enabled = false;
            tbQuestion.Enabled = false;
        }

        #region Click events

        private void btnAddVariable_Click(object sender, System.EventArgs e)
        {
            _addEditVariableForm = new AddEditVariableForm(_knowledgeBase, _variables);
            var result = _addEditVariableForm.ShowDialog();
            if (_addEditVariableForm.NewVariable != null
                && result == DialogResult.OK)
            {
                _variables.Add(_addEditVariableForm.NewVariable);
                InitializeVariables();
                _isChanged = true;
                dgvVariable.CurrentCell = dgvVariable.Rows[dgvVariable.RowCount - 1].Cells[0];
                InitializeVariableProperties(_addEditVariableForm.NewVariable);
            }
        }

        private void btnDeleteVariable_Click(object sender, System.EventArgs e)
        {
            DeleteVariable(((Variable)dgvVariable.CurrentRow?.Tag)?.Name);
        }

        private void btnEditVariable_Click(object sender, System.EventArgs e)
        {
            var variable = (Variable)dgvVariable.CurrentRow?.Tag;

            if (variable.Original == null)
            {
                variable.Original = variable;
            }
            
            _addEditVariableForm = new AddEditVariableForm(_knowledgeBase, _variables, variable);
            var result = _addEditVariableForm.ShowDialog();
            int? rowIndex = dgvVariable.CurrentRow?.Index;
            if (result == DialogResult.OK)
            {
                InitializeVariables();
                if (rowIndex.HasValue)
                {
                    dgvVariable.CurrentCell = dgvVariable.Rows[rowIndex.Value].Cells[0];
                }
                InitializeVariableProperties(variable);
                _isChanged = true;
            }
        }

        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        #endregion

        #region Changed events

        private void dgvVariable_SelectionChanged(object sender, System.EventArgs e)
        {
            InitializeVariableProperties((Variable)dgvVariable.CurrentRow?.Tag);
        }

        #endregion

        #region INIT

        private void InitializeVariables()
        {
            dgvVariable.Rows.Clear();
            foreach (var variable in _variables)
            {
                if (!variable.IsRemoved)
                {
                    var row = new DataGridViewRow();
                    row.CreateCells(dgvVariable, variable.Name);
                    row.Tag = variable;
                    dgvVariable.Rows.Add(row);
                }
            }
        }

        private void InitializeDomains(string domainName = null)
        {
            cbDomain.Items.Clear();
            cbDomain.Items.Add(" ");
            if (_knowledgeBase.Domains.Any())
            {
                cbDomain.Items.AddRange(_knowledgeBase.Domains.Select(d => d.Name).ToArray());
            }

            if (domainName != null)
            {
                cbDomain.SelectedItem = domainName;
            }
        }

        private void InitializeVariableProperties(Variable variable)
        {
            if (variable == null)
            {
                return;
            }
            cbDomain.SelectedItem = variable.Domain.Name ?? " ";
            tbQuestion.Text = variable.Question;
            switch (variable.Type)
            {
                case tVariableType.Requested:
                    rbRequested.Checked = true;
                    break;
                case tVariableType.Derivable:
                    rbDerivable.Checked = true;
                    break;
                case tVariableType.DerivableRequested:
                    rbDeriveRequest.Checked = true;
                    break;
            }
        }

        private void InitVariableList()
        {
            _variables.Clear();
            foreach (var variable in _knowledgeBase.Variables)
            {
                _variables.Add(new Variable(variable.Name, variable.Domain, variable.Type, variable.Question, variable));
            }
        }

        #endregion

        #region DELETE

        private void DeleteVariable(string variableName)
        {
            if (variableName != null)
            {
                DialogResult result = DialogResult.Cancel;
                if (_knowledgeBase.GetRuleByVariable(variableName) != null)
                {
                    result = MessageBox.Show("Эта переменная используется в правилах! При изменении будут удалены все правила, где она используется. Продолжить?"
                        , "Внимание", MessageBoxButtons.OKCancel);
                }
                else
                {
                    var variable = _variables.FirstOrDefault(v => v.Name == variableName);
                    dgvVariable.Rows.Remove(dgvVariable.CurrentRow);
                    variable.IsRemoved = true;
                    _isChanged = true;
                }

                if (result == DialogResult.OK)
                {
                    var variable = _variables.FirstOrDefault(v => v.Name == variableName);
                    dgvVariable.Rows.Remove(dgvVariable.CurrentRow);
                    variable.IsRemoved = true;
                    _isChanged = true;
                }
            }
        }

        #endregion

        #region Closing events

        private void VariablesForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_isChanged)
            {
                var result = MessageBox.Show(
                    "Сохранить изменения?",
                    "Внимание",
                    MessageBoxButtons.YesNoCancel);

                if (result == DialogResult.Yes)
                {
                    _knowledgeBase.ReplaceVariables(_variables);
                }

                e.Cancel = result == DialogResult.Cancel;
            }
        }

        #endregion
    }
}